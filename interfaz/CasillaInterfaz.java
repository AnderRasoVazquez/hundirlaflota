package interfaz;

import codigo.Casilla;
import codigo.Juego;
import codigo.Jugador;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

@SuppressWarnings("serial")
public class CasillaInterfaz extends JPanel {

    private final LineBorder BORDE_SELECCION = new LineBorder(new Color(255, 0, 0), 2);

    private boolean oculta;
    private String contenido;
    private Casilla coordenadas;
    private JLabel label;
    private LineBorder borde;
    private Jugador jugador;

    public CasillaInterfaz(int pFila, int pColumna, boolean pOculta, Jugador pJugador, Jugador pOponente) {
        oculta = pOculta;
        // oculta = false;
        contenido = "Agua";
        coordenadas = new Casilla(pFila, pColumna);
        setLayout(new BorderLayout(0, 0));
        label = new JLabel();
        this.add(label);
        jugador = pJugador;
        this.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                if (isEnabled()) {
                    if (Juego.getElJuego().getJ1().equals(pOponente)) {
                        Juego.getElJuego().getJ1().atacar(coordenadas.getLocation());
                    } else if (VentanaJuego.getVentanaJuego().getEstadoEscudo()) {
                        if (!Juego.getElJuego().getJ1().activarEscudo(coordenadas.getLocation())) {
                            VentanaJuego.infoBox("No puedes activar el escudo.",
                                    "Activar Escudo");
                        }
                    } else if (VentanaJuego.getVentanaJuego().getEstadoReparacion()) {
                        if (!Juego.getElJuego().getJ1().repararBarco(coordenadas.getLocation())) {
                            VentanaJuego.infoBox("No puedes reparar el barco.",
                                    "Reparar Barco");
                        }
                    }
                }
            }

            public void mouseEntered(MouseEvent e) {
                if (isEnabled())
                    setBorder(BORDE_SELECCION);
                
                if (Juego.getElJuego().getJ1().equals(pOponente)) {
                	setCursor(VentanaJuego.cursorMira);
                } else if (VentanaJuego.getVentanaJuego().getEstadoEscudo()) {
                	setCursor(VentanaJuego.cursorEscudo);
                } else if (VentanaJuego.getVentanaJuego().getEstadoReparacion()) {
                	setCursor(VentanaJuego.cursorLlaveInglesa);
                }
            }
            public void mouseExited(MouseEvent e) {
                if (isEnabled())
                    setBorder(borde);
            }
        });
        actualizarIcono();
    }

    public void bordePreferido(LineBorder pBorde) {
        borde = pBorde;
        setBorder(pBorde);
    }

    public void revelar() {
        oculta = false;
        actualizar();
    }

    public void setContenido(String pContenido) {
        contenido = pContenido;
        actualizarIcono();
    }

    private void actualizarIcono() {
        label.setIcon(new ImageIcon(CasillaInterfaz.class.getResource(oculta ? "/recursos/CAguaNiebla.png"
                                                                      : "/recursos/C" + contenido + ".png")));
    }

    public void actualizar() {
        if (jugador != null && jugador.getFlota() != null) {
            contenido = jugador.getFlota().getContenido(coordenadas.getLocation()).toString();
        }
        actualizarIcono();
    }
}
