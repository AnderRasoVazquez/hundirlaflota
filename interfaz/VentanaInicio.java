package interfaz;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import codigo.Juego;

@SuppressWarnings("serial")
public class VentanaInicio extends JFrame {

    private static VentanaInicio laVentanaInicio;

    public static VentanaInicio getVentanaInicio() {
        if (laVentanaInicio == null) {
            laVentanaInicio = new VentanaInicio();
        }
        return laVentanaInicio;
    }

    private JLabel lblImagen;
    private JLabel lblNombre;
    private JTextField txtFldNombre;
    private JButton btnJugar;
    private JButton btnSalir;
    private final JPanel contentPanel = new JPanel();

    /**
     * Create the dialog.
     */
    private VentanaInicio() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(
                VentanaJuego.class.getResource("/recursos/IHundirLaFlota.png")));
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 500, 500);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(null);
        contentPanel.add(getBtnJugar());
        contentPanel.add(getBtnSalir());
        contentPanel.add(getLblNombre());
        contentPanel.add(getTxtFldNombre());
        contentPanel.add(getLblImagen());
    }

    private JLabel getLblImagen() {
        if (lblImagen == null) {
            lblImagen = new JLabel();
            lblImagen.setIcon(new ImageIcon(VentanaInicio.class.getResource("/recursos/battleships.png")));
            lblImagen.setBounds(0, 0, 484, 461);
        }
        return lblImagen;
    }

    private JLabel getLblNombre() {
        if (lblNombre == null) {
            lblNombre = new JLabel("Nombre", SwingConstants.CENTER);
            lblNombre.setBounds(155,320, 90, 25);
        }
        return lblNombre;
    }

    private JTextField getTxtFldNombre() {
        if (txtFldNombre == null) {
            txtFldNombre = new JTextField();
            txtFldNombre.setBounds(155, 350, 90, 25);
        }
        return txtFldNombre;
    }

    private JButton getBtnJugar() {
        if (btnJugar == null) {
            btnJugar = new JButton("");
            btnJugar.setBounds(170, 380, 62, 46);
            btnJugar.setSelectedIcon(new ImageIcon(VentanaInicio.class.getResource("/recursos/play.png")));
            btnJugar.setIcon(new ImageIcon(VentanaInicio.class.getResource("/recursos/play.png")));
            btnJugar.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    // se crea el Juego
                    String nombre = getTxtFldNombre().getText();
                    if (nombre.isEmpty()) {
                        nombre = "Metatron";
                    }
                    Juego.getElJuego(nombre, "CPU");
                    // se crea el Frame del juego
                    VentanaJuego.getVentanaJuego().comenzar();
                    // se crea el Frame de colocar barcos
                    VentanaColocarBarcos.getVentanaColocarBarcos();
                    // se inicializa el juego
                    Juego.getElJuego().inicializar();
                    setVisible(false);
                    dispose();
                }
            });
        }
        return btnJugar;
    }

    private JButton getBtnSalir() {
        if (btnSalir == null) {
            btnSalir = new JButton("");
            btnSalir.setBounds(263, 380, 56, 46);
            btnSalir.setIcon(new ImageIcon(VentanaInicio.class.getResource("/recursos/exit.png")));
            btnSalir.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    dispose();
                }
            });
        }
        return btnSalir;
    }

    public void comenzar() {
        setVisible(true);
    }

}