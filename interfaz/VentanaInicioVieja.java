package interfaz;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import codigo.Juego;

@SuppressWarnings("serial")
public class VentanaInicioVieja extends JDialog {

    private static VentanaInicioVieja laVentanaInicioVieja;

    public static VentanaInicioVieja getVentanaInicioVieja() {
        if (laVentanaInicioVieja == null) {
            laVentanaInicioVieja = new VentanaInicioVieja();
        }
        return laVentanaInicioVieja;
    }

    private JLabel lblTexto;
    private JLabel lblNombre;
    private JTextField txtFldNombre;
    private JButton btnJugar;
    private JButton btnSalir;
    private final JPanel contentPanel = new JPanel();

    private VentanaInicioVieja() {
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 270, 215);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(null);
        contentPanel.add(getBtnJugar());
        contentPanel.add(getBtnSalir());
        contentPanel.add(getLblTexto());
        contentPanel.add(getLblNombre());
        contentPanel.add(getTxtFldNombre());
    }

    private JLabel getLblTexto() {
        if (lblTexto == null) {
            lblTexto = new JLabel(
                                         "<html><body><center>Bienvenido a Hundir la Flota.<br>Esta es la versión 0.1 del juego.<br>¿Qué desea hacer?</center></body></html>");
            lblTexto.setBounds(30, 10, 210, 85);
        }
        return lblTexto;
    }

    private JLabel getLblNombre() {
        if (lblNombre == null) {
            lblNombre = new JLabel("Nombre", SwingConstants.CENTER);
            lblNombre.setBounds(10, 110, 90, 25);
        }
        return lblNombre;
    }

    private JTextField getTxtFldNombre() {
        if (txtFldNombre == null) {
            txtFldNombre = new JTextField();
            txtFldNombre.setBounds(10, 145, 90, 25);
        }
        return txtFldNombre;
    }

    private JButton getBtnJugar() {
        if (btnJugar == null) {
            btnJugar = new JButton("Jugar");
            btnJugar.setBounds(10, 180, 90, 25);
            btnJugar.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    // se crea el Juego
                    String nombre = getTxtFldNombre().getText();
                    if (nombre.isEmpty()) {
                        nombre = "Metatron";
                    }
                    Juego.getElJuego(nombre, "CPU");
                    // se crea el Frame del juego
                    VentanaJuego.getVentanaJuego().comenzar();
                    // se crea el Frame de colocar barcos
                    VentanaColocarBarcos.getVentanaColocarBarcos();
                    // se inicializa el juego
                    Juego.getElJuego().inicializar();
                    setVisible(false);
                    dispose();
                }
            });
        }
        return btnJugar;
    }

    private JButton getBtnSalir() {
        if (btnSalir == null) {
            btnSalir = new JButton("Salir");
            btnSalir.setBounds(152, 180, 90, 25);
            btnSalir.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    dispose();
                }
            });
        }
        return btnSalir;
    }


    public void comenzar() {
        setVisible(true);
    }
}
