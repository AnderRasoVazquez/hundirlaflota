package interfaz;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.Collection;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import codigo.Arsenal;
import codigo.Casilla;
import codigo.Flota;
import codigo.Juego;
import codigo.Jugador;
import codigo.NotifyArg;

@SuppressWarnings("serial")
public class Tablero extends JPanel implements Observer {

    private final LineBorder BORDE_CUADRICULA = new LineBorder(new Color(0, 0, 0), 1);

    CasillaInterfaz[][] casillas = new CasillaInterfaz[Juego.ALTURA_TABLERO][Juego.ANCHURA_TABLERO];
    private Jugador oponente;

    public Tablero(int pX, int pY, int pWidth, int pHeight, boolean pOculto, Jugador pJugador,
                   Jugador pOponente) {
        setBackground(Color.WHITE);
        setBorder(new LineBorder(new Color(0, 0, 0), 2));
        setBounds(pX, pY, pWidth, pHeight);
        setLayout(new GridLayout(Juego.ALTURA_TABLERO + 1, Juego.ANCHURA_TABLERO + 1, 0, 0));
        for (int fila = -1; fila < Juego.ALTURA_TABLERO; fila++) {
            if (fila >= 0) {
                add(new JLabel(Integer.toString(fila), SwingConstants.CENTER));
                for (int col = 0; col < Juego.ANCHURA_TABLERO; col++) {
                    add(casillas[fila][col] = new CasillaInterfaz(fila, col, pOculto, pJugador, pOponente));
                }
            } else {
                add(new JLabel());
                for (int col = 0; col < Juego.ANCHURA_TABLERO; col++) {
                    add(new JLabel(Integer.toString(col), SwingConstants.CENTER));
                }
            }
        }
        oponente = pOponente;
        pJugador.getFlota().addObserver(this);
        pJugador.getArsenal().addObserver(this);
        pOponente.getArsenal().addObserver(this);
    }

    public void activarCuadricula() {
        for (CasillaInterfaz[] fila : casillas) {
            for (CasillaInterfaz casilla : fila) {
                casilla.bordePreferido(BORDE_CUADRICULA);
            }
        }
    }

    public void desactivarCuadricula() {
        for (CasillaInterfaz[] fila : casillas) {
            for (CasillaInterfaz casilla : fila) {
                casilla.bordePreferido(null);
            }
        }
    }

    public void revelarTodas() {
        for (CasillaInterfaz[] linea : casillas) {
            for (CasillaInterfaz casilla : linea) {
                casilla.revelar();
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void update(Observable arg0, Object arg1) {
        // observa a los dos Arsenales y a Flota (del jugador)
        String motivo = ((NotifyArg) arg1).getMotivo();
        Object arg = ((NotifyArg) arg1).getArg();
        if (arg0 instanceof Arsenal) { // notify de Arsenal (arsenal del oponente)
            if (motivo != "reabastecer") {
                // si no es un reabastecimiento de munición
                // entonces es un disparo, reparación o escudo
                // se actualiza el contenido de todas las casillas
                for (CasillaInterfaz[] linea : casillas) {
                    for (CasillaInterfaz casilla : linea) {
                        casilla.actualizar();
                    }
                }
                if (motivo == "disparar" && arg0.equals(oponente.getArsenal())) {
                    // si además ha sido un disparo del oponente
                    // se revelan las casillas disparadas
                    int fila, columna;
                    for (Casilla casilla : (Collection<Casilla>) arg) {
                        fila = (int) casilla.getX();
                        columna = (int) casilla.getY();
                        casillas[fila][columna].revelar();
                    }
                }
            }
        } else if (arg0 instanceof Flota) { // notify de Flota (flota del jugador)
            if (motivo == "colocarBarco") {
                // si se ha colocado un barco
                // se actualiza el contenido de las casillas afectadas
                int fila, columna;
                for (Casilla casilla : (Collection<Casilla>) arg) {
                    fila = (int) casilla.getX();
                    columna = (int) casilla.getY();
                    casillas[fila][columna].actualizar();
                }
            } else if (motivo == "recogerBarcos") {
                // si se han recogido todos los barcos
                // se actualiza el contenido de todas las casillas
                for (CasillaInterfaz[] linea : casillas) {
                    for (CasillaInterfaz casilla : linea) {
                        casilla.actualizar();
                    }
                }
            }
        }
    }
}
