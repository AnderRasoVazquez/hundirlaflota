package interfaz;

import codigo.Casilla;
import codigo.Juego;
import codigo.Jugador;
import codigo.NotifyArg;
import codigo.armas.Armamento;

import javax.swing.*;
import javax.swing.text.DefaultCaret;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

@SuppressWarnings("serial")
public class VentanaJuego extends JFrame implements Observer {

    private static VentanaJuego laVentanaJuego;

    public static VentanaJuego getVentanaJuego() {
        if (laVentanaJuego == null) {
            laVentanaJuego = new VentanaJuego();
        }
        return laVentanaJuego;
    }

    /**
     * Elementos WindowBuilder
     */
    private Tablero tableroAliado;
    private Tablero tableroEnemigo;
    private PanelArsenal panelArsenal;
    private JScrollPane scrollPane;
    private JTextArea txtConsole;
    private JButton btnFinTurno;
    private JMenuBar barraMenu;
    private JMenu mnJuego;
    private JMenu mnOpciones;
    private JMenu mnAyuda;
    private JMenuItem mntmNuevoJuego;
    private JMenuItem mntmPuntuaciones;
    private JMenuItem mntmSalir;
    private JCheckBoxMenuItem checkboxCuadricula;
    private JMenuItem mntmAcercaDe;
    private JMenuItem mntmInstrucciones;
    
    static public Cursor cursorNormal = new Cursor(Cursor.DEFAULT_CURSOR);
    static public Cursor cursorMano = new Cursor(Cursor.HAND_CURSOR);
    static public Cursor cursorMira = Toolkit.getDefaultToolkit().createCustomCursor(
			new ImageIcon(CasillaInterfaz.class.getResource("/recursos/CursorMira.png")).getImage(),
			new Point(16,16),"cursorMira");
    static public Cursor cursorEscudo = Toolkit.getDefaultToolkit().createCustomCursor(
			new ImageIcon(CasillaInterfaz.class.getResource("/recursos/CursorEscudo.png")).getImage(),
			new Point(3,3),"cursorEscudo");
    static public Cursor cursorLlaveInglesa = Toolkit.getDefaultToolkit().createCustomCursor(
			new ImageIcon(CasillaInterfaz.class.getResource("/recursos/CursorLlaveInglesa.png")).getImage(),
			new Point(3,3),"cursorLlaveInglesa");

    /**
     * Crea la aplicación.
     */
    private VentanaJuego() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(
                VentanaJuego.class.getResource("/recursos/IHundirLaFlota.png")));
        getContentPane().setBackground(SystemColor.control);
        getContentPane().setForeground(Color.BLACK);
        setTitle("Hundir la Flota v0.1");
        setBounds(100, 50, 825, 675);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);
        getContentPane().add(getTableroAliado());
        getContentPane().add(getTableroEnemigo());
        getContentPane().add(getPanelArsenal());
        getContentPane().add(getScrollPane());
        getContentPane().add(getBtnFinTurno());
        getContentPane().setFocusTraversalPolicy(
                new FocusTraversalOnArray(new Component[]{getTableroAliado(), getPanelArsenal()}));
        setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{mnJuego,
                getMnOpciones(), mnAyuda, getPanelArsenal()}));
        setJMenuBar(getBarraMenu());

        Juego.getElJuego().addObserver(this);
        Juego.getElJuego().getJ1().addObserver(this);
        Juego.getElJuego().getJ2().addObserver(this);
    }

    /*
     * A continuación se encuentran los paneles de cada uno de los jugadores
     */
    private Tablero getTableroAliado() {
        if (tableroAliado == null) {
            tableroAliado = new Tablero(10, 15, 385, 385, false, Juego.getElJuego().getJ1(), Juego.getElJuego().getJ2());
            tableroAliado.addMouseListener(new MouseAdapter() {
                public void mouseEntered(MouseEvent e) {
                    if (isEnabled())
                        setCursor(cursorNormal);
            	}
            });
        }
        return tableroAliado;
    }

    private Tablero getTableroEnemigo() {
        if (tableroEnemigo == null) {
            tableroEnemigo = new Tablero(414, 15, 385, 385, true, Juego.getElJuego().getJ2(), Juego.getElJuego().getJ1());
            tableroAliado.addMouseListener(new MouseAdapter() {
                public void mouseEntered(MouseEvent e) {
                    if (isEnabled())
                        setCursor(cursorNormal);
            	}
            });
        }
        return tableroEnemigo;
    }

    private PanelArsenal getPanelArsenal() {
        if (panelArsenal == null) {
            panelArsenal = new PanelArsenal(10, 422, 500, 182, Juego.getElJuego().getJ1().getArsenal());
        }
        return panelArsenal;
    }

    /*
     * A continuación se encuentra el cuadro dinámico de texto del juego y su panel correspondiente.
     */
    private JScrollPane getScrollPane() {
        if (scrollPane == null) {
            scrollPane = new JScrollPane();
            scrollPane.setBounds(524, 422, 275, 136);
            scrollPane.setViewportView(getTxtConsole());
        }
        return scrollPane;
    }

    public JTextArea getTxtConsole() {
        if (txtConsole == null) {
            txtConsole = new JTextArea();
            txtConsole.setEditable(false);
            DefaultCaret caret = (DefaultCaret) txtConsole.getCaret();
            caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
            txtConsole.setTabSize(3);
            txtConsole.setWrapStyleWord(true);
            txtConsole.setBackground(SystemColor.control);
            txtConsole.setForeground(Color.BLACK);
            txtConsole.setLineWrap(true);
            txtConsole.setFont(new Font("Arial", Font.PLAIN, 12));
            txtConsole
                    .append("Bienvenido a Hundir la Flota.\r\nEste programa ha sido hecho por:\r\n\tAnder Raso\r\n\tJon Peña\r\n\tDavid Pérez\r\n\tGuzmán López\r\n\r\n");
        }
        return txtConsole;
    }

    private JButton getBtnFinTurno() {
        if (btnFinTurno == null) {
            btnFinTurno = new JButton("Fin Turno");
            btnFinTurno.addMouseListener(new MouseAdapter() {
            	public void mouseEntered(MouseEvent arg0) {
            		setCursor(cursorMano);
            	}
            	public void mouseExited(MouseEvent arg0){
            		setCursor(cursorNormal);
            	}
            });
            btnFinTurno.setBounds(524, 569, 275, 35);
            btnFinTurno.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    panelArsenal.desactivarBotones();
                    Juego.getElJuego().getJ1().terminarTurno();
                }
            });
        }
        return btnFinTurno;
    }

    /*
     * A continuación se encuentra la barra menu.
     */
    private JMenuBar getBarraMenu() {
        if (barraMenu == null) {
            barraMenu = new JMenuBar();
            barraMenu.add(getMnJuego());
            barraMenu.add(getMnOpciones());
            barraMenu.add(getMnAyuda());
        }
        return barraMenu;
    }

    private JMenu getMnJuego() {
        if (mnJuego == null) {
            mnJuego = new JMenu("Juego");
            mnJuego.add(getMntmNuevoJuego());
            mnJuego.add(getMntmPuntuaciones());
            mnJuego.add(getMntmSalir());
        }
        return mnJuego;
    }

    private JMenu getMnOpciones() {
        if (mnOpciones == null) {
            mnOpciones = new JMenu("Opciones");
            mnOpciones.add(getCheckboxCuadricula());
        }
        return mnOpciones;
    }

    private JMenu getMnAyuda() {
        if (mnAyuda == null) {
            mnAyuda = new JMenu("Ayuda");
            mnAyuda.add(getMntmInstrucciones());
            mnAyuda.add(getMntmAcercaDe());
        }
        return mnAyuda;
    }

    private JMenuItem getMntmNuevoJuego() {
        if (mntmNuevoJuego == null) {
            mntmNuevoJuego = new JMenuItem("Nuevo juego");
            mntmNuevoJuego.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    // TODO metodo sin implementar
                }
            });
        }
        return mntmNuevoJuego;
    }

    private JMenuItem getMntmPuntuaciones() {
        if (mntmPuntuaciones == null) {
            mntmPuntuaciones = new JMenuItem("Puntuaciones");
            mntmPuntuaciones.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    // TODO metodo sin implementar
                }
            });
        }
        return mntmPuntuaciones;
    }

    private JMenuItem getMntmSalir() {
        if (mntmSalir == null) {
            mntmSalir = new JMenuItem("Salir");
            mntmSalir.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    dispose();
                }
            });
        }
        return mntmSalir;
    }

    private JCheckBoxMenuItem getCheckboxCuadricula() {
        if (checkboxCuadricula == null) {
            checkboxCuadricula = new JCheckBoxMenuItem("Mostrar cuadrícula");
            checkboxCuadricula.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    if (checkboxCuadricula.getState()) {
                        getTableroAliado().activarCuadricula();
                        getTableroEnemigo().activarCuadricula();
                    } else {
                        getTableroAliado().desactivarCuadricula();
                        getTableroEnemigo().desactivarCuadricula();
                    }
                }
            });
        }
        return checkboxCuadricula;
    }

    private JMenuItem getMntmAcercaDe() {
        if (mntmAcercaDe == null) {
            mntmAcercaDe = new JMenuItem("Acerca de...");
            mntmAcercaDe.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    // TODO metodo sin implementar
                }
            });
        }
        return mntmAcercaDe;
    }

    private JMenuItem getMntmInstrucciones() {
        if (mntmInstrucciones == null) {
            mntmInstrucciones = new JMenuItem("Instrucciones");
            mntmInstrucciones.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    new Instrucciones();
                }
            });
        }
        return mntmInstrucciones;
    }

    public void comenzar() {
        setVisible(true);
    }

    public static void infoBox(String infoMessage, String titleBar) {
        JOptionPane.showMessageDialog(null, infoMessage, "InfoBox: " + titleBar,
                JOptionPane.INFORMATION_MESSAGE);
    }

    public static String inputBox(String infoMessage, String titleBar) {
        return JOptionPane.showInputDialog(null, infoMessage, "InputBox: " + titleBar,
                JOptionPane.INFORMATION_MESSAGE);
    }

    public boolean getEstadoReparacion() {
        return getPanelArsenal().getEstadoReparacion();
    }

    public boolean getEstadoEscudo() {
        return getPanelArsenal().getEstadoEscudo();
    }

    private void finJuego(String pMensaje) {
        infoBox(pMensaje, "¡FIN DEL JUEGO!");
        getTableroEnemigo().revelarTodas();
    }

    @Override
    public void update(Observable arg0, Object arg1) {
        // observa a la instancia del Juego y a los dos jugadores
        String motivo = ((NotifyArg) arg1).getMotivo();
        Object arg = ((NotifyArg) arg1).getArg();
        if (motivo == "atacar") {
            Casilla casilla = (Casilla) arg;
            getTxtConsole().append(((Jugador) arg0).getNombre() + " dispara a la casilla "
                                           + casilla.toString() + " con "
                                           + ((Jugador) arg0).getArsenal().getArmaEquipada().getNombre() + ".\n");
        } else if (motivo == "equiparArma" && arg0.equals(Juego.getElJuego().getJ1())) {
            getTxtConsole().append("Arma equipada: " + ((Armamento) arg).getNombre() + ".\n");
        } else if (motivo == "repararBarco" && arg0.equals(Juego.getElJuego().getJ1())) {
            Casilla casilla = (Casilla) arg;
            getTxtConsole().append("Reparado el barco en la casilla " + casilla.toString() + ".\n");
        } else if (motivo == "activarEscudo" && arg0.equals(Juego.getElJuego().getJ1())) {
            Casilla casilla = (Casilla) arg;
            getTxtConsole().append("Escudado el barco en la casilla " + casilla.toString() + ".\n");
        } else if (motivo == "cambiarTurno") {
            getTxtConsole().append("\nTurno " + Integer.toString(Juego.getElJuego().getTurno()) + " - " +
                                           arg.toString() + "\n");
        } else if (motivo == "finJuego") {
            finJuego(arg.toString());
        }
    }
}
