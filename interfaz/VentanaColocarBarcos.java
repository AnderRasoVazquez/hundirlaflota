package interfaz;

import codigo.Casilla;
import codigo.Flota;
import codigo.Juego;
import codigo.Orientacion;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.util.Observable;
import java.util.Observer;

@SuppressWarnings("serial")
public class VentanaColocarBarcos extends JFrame implements Observer {

    private static VentanaColocarBarcos laVentanaColocarBarcos;

    public static VentanaColocarBarcos getVentanaColocarBarcos() {
        if (laVentanaColocarBarcos == null) {
            laVentanaColocarBarcos = new VentanaColocarBarcos();
        }
        return laVentanaColocarBarcos;
    }

    private Flota flota;
    private Integer[] restantes;
    private CasillaInterfaz[] casillas = new CasillaInterfaz[6];
    private JComboBox<String> comboBoxTipoBarco;
    private JLabel lblTipoDeBarco;
    private JLabel lblFila;
    private JLabel lblColumna;
    private JLabel lblOrientacin;
    private JTextField txtFldFila;
    private JTextField txtFldColumna;
    private JButton btnColocar;
    private JRadioButton rdbtnNorte;
    private JRadioButton rdbtnSur;
    private JRadioButton rdbtnEste;
    private JRadioButton rdbtnOeste;
    private final ButtonGroup btnGrpOrientacion = new ButtonGroup();
    private JPanel pnlMuestra;
    private JButton btnCancelar;
    private JTextField txtFldRestantes;
    private JButton btnAuto;
    private JButton btnRecoger;

    /**
     * Create the dialog.
     */
    public VentanaColocarBarcos() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(
                VentanaJuego.class.getResource("/recursos/IHundirLaFlota.png")));
        setResizable(false);
        setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        setBounds(500, 100, 400, 275);
        getContentPane().setLayout(null);
        getContentPane().add(getComboBoxTipoBarco());
        getContentPane().add(getTxtFldRestantes());
        getContentPane().add(getLblTipoDeBarco());
        getContentPane().add(getLblFila());
        getContentPane().add(getLblColumna());
        getContentPane().add(getLblOrientacin());
        getContentPane().add(getTxtFldFila());
        getContentPane().add(getTxtFldColumna());
        getContentPane().add(getBtnColocar());
        getContentPane().add(getBtnCancelar());
        getContentPane().add(getBtnAuto());
        getContentPane().add(getRdbtnNorte());
        getContentPane().add(getRdbtnSur());
        getContentPane().add(getRdbtnEste());
        getContentPane().add(getRdbtnOeste());
        getContentPane().add(getPnlMuestra());
        getContentPane().add(getBtnRecoger());

        Juego.getElJuego().getJ1().addObserver(this);
    }

    private JPanel getPnlMuestra() {
        if (pnlMuestra == null) {
            pnlMuestra = new JPanel();
            pnlMuestra.setBounds(87, 25, 210, 35);
            pnlMuestra.setLayout(new GridLayout(1, 6, 0, 0));
            for (int i = 0; i < 6; i++) {
                casillas[i] = new CasillaInterfaz(0, i, true, null, null);
                casillas[i].setEnabled(false);
                pnlMuestra.add(casillas[i]);
            }
        }
        return pnlMuestra;
    }

    private JComboBox<String> getComboBoxTipoBarco() {
        if (comboBoxTipoBarco == null) {
            comboBoxTipoBarco = new JComboBox<String>();
            comboBoxTipoBarco.addItemListener(new ItemListener() {
                public void itemStateChanged(ItemEvent arg0) {
                    actualizarSeleccion();
                }
            });
            comboBoxTipoBarco.setModel(new DefaultComboBoxModel<String>(new String[]{"", "Fragata",
                    "Destructor", "Submarino", "Portaaviones"}));
            comboBoxTipoBarco.setBounds(183, 75, 120, 20);
        }
        return comboBoxTipoBarco;
    }

    private JLabel getLblTipoDeBarco() {
        if (lblTipoDeBarco == null) {
            lblTipoDeBarco = new JLabel("Tipo de Barco:");
            lblTipoDeBarco.setBounds(35, 75, 100, 20);
        }
        return lblTipoDeBarco;
    }

    private JLabel getLblFila() {
        if (lblFila == null) {
            lblFila = new JLabel("Fila:");
            lblFila.setBounds(35, 100, 100, 20);
        }
        return lblFila;
    }

    private JLabel getLblColumna() {
        if (lblColumna == null) {
            lblColumna = new JLabel("Columna:");
            lblColumna.setBounds(35, 125, 100, 20);
        }
        return lblColumna;
    }

    private JLabel getLblOrientacin() {
        if (lblOrientacin == null) {
            lblOrientacin = new JLabel("Orientación:");
            lblOrientacin.setBounds(35, 150, 100, 20);
        }
        return lblOrientacin;
    }

    private JTextField getTxtFldRestantes() {
        if (txtFldRestantes == null) {
            txtFldRestantes = new JTextField();
            txtFldRestantes.setHorizontalAlignment(SwingConstants.CENTER);
            txtFldRestantes.setEditable(false);
            txtFldRestantes.setColumns(10);
            txtFldRestantes.setBounds(313, 75, 49, 20);
        }
        return txtFldRestantes;
    }

    private JTextField getTxtFldFila() {
        if (txtFldFila == null) {
            txtFldFila = new JTextField();
            txtFldFila.setBounds(183, 100, 120, 20);
            txtFldFila.setColumns(10);
        }
        return txtFldFila;
    }

    private JTextField getTxtFldColumna() {
        if (txtFldColumna == null) {
            txtFldColumna = new JTextField();
            txtFldColumna.setColumns(10);
            txtFldColumna.setBounds(183, 125, 120, 20);
        }
        return txtFldColumna;
    }

    private JButton getBtnColocar() {
        if (btnColocar == null) {
            btnColocar = new JButton("Colocar");
            btnColocar.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent arg0) {
                    int fila, columna;
                    try {
                        fila = Integer.parseInt(getTxtFldFila().getText());
                        columna = Integer.parseInt(getTxtFldColumna().getText());
                    } catch (NumberFormatException e) {
                        fila = -1;
                        columna = -1;
                    }
                    Casilla casilla = new Casilla(fila, columna);
                    Orientacion orientacion = getOrientacion();
                    if (flota.colocarBarco(Flota.encontrarCasillas(casilla, orientacion, getTamanoBarco()))) {
                        VentanaJuego.infoBox("El barco ha sido colocado correctamente",
                                "Barco colocado");
                        actualizarRestantes();
                        limpiarTexto();
                        if (!quedanRestantes()) {
                            salir();
                        }
                    } else {
                        VentanaJuego.infoBox("El barco no se ha podido colocar",
                                "Error al colocar el barco");
                    }
                }
            });
            btnColocar.setBounds(35, 202, 85, 23);
        }
        return btnColocar;
    }

    private JButton getBtnCancelar() {
        if (btnCancelar == null) {
            btnCancelar = new JButton("Cancelar");
            btnCancelar.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    cancelar();
                }
            });
            btnCancelar.setBounds(153, 202, 85, 23);
        }
        return btnCancelar;
    }

    private JButton getBtnRecoger() {
        if (btnRecoger == null) {
            btnRecoger = new JButton("Recoger");
            btnRecoger.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    flota.recogerBarcos();
                    actualizarRestantes();
                }
            });
            btnRecoger.setBounds(269, 202, 85, 23);
        }
        return btnRecoger;
    }

    private JButton getBtnAuto() {
        if (btnAuto == null) {
            btnAuto = new JButton("Auto");
            btnAuto.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    VentanaJuego.infoBox("Los barcos se han colocado de forma automática en una" +
                                             " disposición predeterminada",
                            "Colocación automática");
                    flota.colocacionControlada();
                    salir();
                }
            });
            btnAuto.setBounds(10, 25, 61, 23);
        }
        return btnAuto;
    }

    private JRadioButton getRdbtnNorte() {
        if (rdbtnNorte == null) {
            rdbtnNorte = new JRadioButton("Norte");
            btnGrpOrientacion.add(rdbtnNorte);
            rdbtnNorte.setBounds(131, 150, 65, 20);
            rdbtnNorte.setSelected(true);
        }
        return rdbtnNorte;
    }

    private JRadioButton getRdbtnSur() {
        if (rdbtnSur == null) {
            rdbtnSur = new JRadioButton("Sur");
            btnGrpOrientacion.add(rdbtnSur);
            rdbtnSur.setBounds(193, 150, 45, 20);
        }
        return rdbtnSur;
    }

    private JRadioButton getRdbtnEste() {
        if (rdbtnEste == null) {
            rdbtnEste = new JRadioButton("Este");
            btnGrpOrientacion.add(rdbtnEste);
            rdbtnEste.setBounds(245, 150, 50, 20);
        }
        return rdbtnEste;
    }

    private JRadioButton getRdbtnOeste() {
        if (rdbtnOeste == null) {
            rdbtnOeste = new JRadioButton("Oeste");
            btnGrpOrientacion.add(rdbtnOeste);
            rdbtnOeste.setBounds(297, 150, 65, 20);
        }
        return rdbtnOeste;
    }

    private Orientacion getOrientacion() {
        if (getRdbtnNorte().isSelected()) {
            return Orientacion.NORTE;
        } else if (getRdbtnSur().isSelected()) {
            return Orientacion.SUR;
        } else if (getRdbtnEste().isSelected()) {
            return Orientacion.ESTE;
        } else if (getRdbtnOeste().isSelected()) {
            return Orientacion.OESTE;
        } else {
            return null;
        }
    }

    private int getTamanoBarco() {
        switch (((String) getComboBoxTipoBarco().getSelectedItem()).toLowerCase()) {
            case "portaaviones":
                return 4;
            case "submarino":
                return 3;
            case "destructor":
                return 2;
            case "fragata":
                return 1;
            default:
                return 0;
        }
    }

    public void colocarBarcos() {
        setVisible(true);
        VentanaJuego.getVentanaJuego().setEnabled(false);
        flota = Juego.getElJuego().getJ1().getFlota();
        actualizarRestantes();
    }

    private boolean quedanRestantes() {
        boolean res = false;
        for (int num : restantes) {
            res = res || num != 0;
        }
        return res;
    }

    private void actualizarRestantes() {
        restantes = new Integer[4];
        for (int i = 0; i < restantes.length; i++) {
            restantes[i] = new Integer(flota.restantes(i + 1));
        }
        actualizarSeleccion();
    }

    private void actualizarSeleccion() {
        switch (((String) comboBoxTipoBarco.getSelectedItem()).toLowerCase()) {
            case "fragata":
                getTxtFldRestantes().setText(restantes[0].toString());
                for (int j = 0; j < 6; j++) {
                    if (j == 2) {
                        casillas[j].setContenido("Barco");
//                        casillas[j].setContenido("F");
                    } else {
                        casillas[j].setContenido("Agua");
                    }
                    casillas[j].revelar();
                }
                break;
            case "destructor":
                getTxtFldRestantes().setText(restantes[1].toString());
                for (int j = 0; j < 6; j++) {
                    if (j == 2) {
                        casillas[j].setContenido("Barco");
//                        casillas[j].setContenido("DE1");
                    } else if (j == 3) {
                        casillas[j].setContenido("Barco");
//                        casillas[j].setContenido("DE2");
                    } else {
                        casillas[j].setContenido("Agua");
                    }
                    casillas[j].revelar();
                }
                break;
            case "submarino":
                getTxtFldRestantes().setText(restantes[2].toString());
                for (int j = 0; j < 6; j++) {
                    if (j == 1) {
                        casillas[j].setContenido("Barco");
//                        casillas[j].setContenido("SE1");
                    } else if (j == 2) {
                        casillas[j].setContenido("Barco");
//                        casillas[j].setContenido("SE2");
                    } else if (j == 3) {
                        casillas[j].setContenido("Barco");
//                        casillas[j].setContenido("SE3");
                    } else {
                        casillas[j].setContenido("Agua");
                    }
                    casillas[j].revelar();
                }
                break;
            case "portaaviones":
                getTxtFldRestantes().setText(restantes[3].toString());
                for (int j = 0; j < 6; j++) {
                    if (j == 1) {
                        casillas[j].setContenido("Barco");
//                        casillas[j].setContenido("PE1");
                    } else if (j == 2) {
                        casillas[j].setContenido("Barco");
//                        casillas[j].setContenido("PE2");
                    } else if (j == 3) {
                        casillas[j].setContenido("Barco");
//                        casillas[j].setContenido("PE3");
                    } else if (j == 4) {
                        casillas[j].setContenido("Barco");
//                        casillas[j].setContenido("PE4");
                    } else {
                        casillas[j].setContenido("Agua");
                    }
                    casillas[j].revelar();
                }
                break;
            default:
                getTxtFldRestantes().setText("");
                for (int j = 0; j < 6; j++) {
                    casillas[j].setContenido("Agua");
                    casillas[j].revelar();
                }
                break;
        }
    }

    private void limpiarTexto() {
        getTxtFldColumna().setText("");
        getTxtFldFila().setText("");
    }

    private void salir() {
        VentanaJuego.getVentanaJuego().setEnabled(true);
        setVisible(false);
        dispose();
    }

    private void cancelar() {
        VentanaJuego.getVentanaJuego().dispose();
        dispose();
    }

    @Override
    public void update(Observable arg0, Object arg1) {
        // observa a Jugador
        colocarBarcos();
        Juego.getElJuego().getJ1().deleteObserver(this);
    }
}
