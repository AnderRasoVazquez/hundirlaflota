package interfaz;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JToggleButton;

@SuppressWarnings("serial")
public class Instrucciones extends JDialog {
    
	private JButton btnOk;
	private JToggleButton tglbtnFlota;
	private JToggleButton tglbtnTurnos;
	private JToggleButton tglbtnArsenal;
	private JToggleButton tglbtnReparaciones;
	private JToggleButton tglbtnIntroduccin;
	private JToggleButton tglbtnTableros;
	private JScrollPane scrollPane;
	private JTextPane txtpnTexto;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JToggleButton tglbtnEscudos;

	/**
	 * Create the dialog.
	 */
	public Instrucciones() {
		setTitle("Instrucciones");
		setBounds(100, 100, 500, 322);
		getContentPane().setLayout(null);
		getContentPane().add(getBtnOk());
		getContentPane().add(getTglbtnFlota());
		getContentPane().add(getTglbtnTurnos());
		getContentPane().add(getTglbtnArsenal());
		getContentPane().add(getTglbtnReparaciones());
		getContentPane().add(getTglbtnIntroduccin());
		getContentPane().add(getTglbtnTableros());
		getContentPane().add(getScrollPane());
		getContentPane().add(getTglbtnEscudos());
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setVisible(true);
	}
	
	private JButton getBtnOk() {
		if (btnOk == null) {
			btnOk = new JButton("OK");
			btnOk.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					dispose();
				}
			});
			btnOk.setActionCommand("OK");
			btnOk.setBounds(404, 249, 70, 23);
		}
		return btnOk;
	}
	
	private JToggleButton getTglbtnFlota() {
		if (tglbtnFlota == null) {
			tglbtnFlota = new JToggleButton("Flota");
			tglbtnFlota.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					txtpnTexto.setText("<b>FLOTA</b>\r\n<br/><br/>\r\nAmbos jugador disponen de un conjunto de barcos, al cual nos referiremos como flota a partir de ahora. Dicha flota se compone de cuatro clases diferentes de barcos:\r\n<br/>\r\n●\tPortaaviones: \tOcupa cuatro casillas, disponemos de uno.<br/>\r\n●\tSubmarino: \tOcupa tres casillas, disponemos de dos.<br/>\r\n●\tFragata: \tOcupa dos casillas, disponemos de tres.<br/>\r\n●\tDestructor:\tOcupa una casilla, disponemos de cuatro.<br/><br/>\r\nAl comienzo de la partida, cada jugador se encarga de distribuir su flota en el tablero con sólo dos limitaciones: no puede haber dos barcos que ocupen una misma casilla, ni puede haber dos barcos colocados de forma que ambos se toquen entre ellos (es decir, con una casilla mínima de distancia entre los dos).\r\n");
					txtpnTexto.setCaretPosition(0);
				}
			});
			buttonGroup.add(tglbtnFlota);
			tglbtnFlota.setBounds(10, 46, 115, 23);
		}
		return tglbtnFlota;
	}
	
	private JToggleButton getTglbtnTurnos() {
		if (tglbtnTurnos == null) {
			tglbtnTurnos = new JToggleButton("Turnos");
			tglbtnTurnos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					txtpnTexto.setText("<b>TURNOS</b>\r\n<br/><br/>\r\nLa partida se divide en una serie de turnos alternados entre los jugadores. Cuando es el turno de un jugador, éste tiene la posibilidad de usar una de sus armas contra el tablero enemigo, reparar uno de sus barcos dañados, proporcionar un escudo a un barco aliado, y reabastecer su arsenal de armas con nuevas unidades.\r\n<br/>\r\nEn un turno se ha de usar obligatoriamente una de las armas contra el tablero enemigo. Sin embargo, solo se puede realizar una reparación o escudar un barco aliado en el mismo turno. Por último, se pueden realizar todas las compras que se deseen.\r\n<br/>\r\nEl final de turno está marcado por el jugador, que deberá seleccionar el botón de “Fin Turno” cuando haya realizado todas las acciones que considere. Tras esto, el turno pasa a ser del enemigo, con las mismas reglas y limitaciones.\r\n<br/>\r\nLos turnos se sucederán hasta que uno de los dos jugadores consiga destruir toda la flota enemiga, momento en el que la partida termina.\r\n");
					txtpnTexto.setCaretPosition(0);
				}
			});
			buttonGroup.add(tglbtnTurnos);
			tglbtnTurnos.setBounds(10, 114, 115, 23);
		}
		return tglbtnTurnos;
	}
	
	private JToggleButton getTglbtnArsenal() {
		if (tglbtnArsenal == null) {
			tglbtnArsenal = new JToggleButton("Arsenal");
			tglbtnArsenal.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					txtpnTexto.setText("<b>ARSENAL</b>\r\n<br/><br/>\r\nEl arsenal de cada jugador consiste en un conjunto de armas que pueden ser utilizadas contra el tablero enemigo. Al comenzar la partida se dispone de una cantidad previa de cada una de las armas, y a lo largo de esta se pueden realizar compras para aumentar la cantidad de un determinado tipo. La cantidad de compras que se realicen está únicamente limitada por el dinero del que dispone el jugador: 1000 monedas de oro.\r\n<br/>\r\nA continuación se detallan las características de las distintas armas de las que dispone cada jugador al comienzo de la partida.\r\n<br/>\r\n●\tBomba: Arma básica del juego. Se dispone de una cantidad ilimitada y no es posible ni necesario comprar más. Hacen falta dos bombas para romper un escudo.\r\n<br/>\r\n●\tMisil: Los misiles son una versión más poderosa de las bombas. Destruyen un barco completo al atacar una de sus casillas, y son capaces de eliminar los escudos de un solo golpe. Se inicia la partida con 3 de ellos y cuesta 100 de oro comprar una unidad.\r\n<br/>\r\n●\tMisil Norte-Sur: Una versión más potente que los misiles, que aplica los efectos de estos en una línea recta vertical que pasa por el punto de disparo. Se comienza la partida con 1 y cuesta 400 de oro adquirir una unidad.\r\n<br/>\r\n●\tMisil Este-Oeste: Una versión más potente que los misiles, que aplica los efectos de estos en una línea recta horizontal que pasa por el punto de disparo. Se comienza la partida con 1 y cuesta 400 de oro comprar una unidad.\r\n<br/>\r\n●\tMisil Boom: Son el arma más poderosa que tiene el jugador. Combina el efecto de los Misiles Norte-Sur y los misiles Este-Oeste, aplicando los efectos de un misil en una cruz con centro en el punto de disparo. Se comienza la partida con 1 y cuesta 600 adquirir una unidad.\r\n<br/>\r\n●\tRadar: El radar se diferencia del resto de armas en que no daña barcos ni escudos. En cambio, al lanzarlo sobre el tablero enemigo, revela al jugador el contenido de todas las casillas en un radio de dos casillas a su alrededor. El radar puede utilizarse tres veces a lo largo de la partida y no es posible adquirir más usos.\r\n");
					txtpnTexto.setCaretPosition(0);
				}
			});
			buttonGroup.add(tglbtnArsenal);
			tglbtnArsenal.setBounds(10, 148, 115, 23);
		}
		return tglbtnArsenal;
	}
	
	private JToggleButton getTglbtnReparaciones() {
		if (tglbtnReparaciones == null) {
			tglbtnReparaciones = new JToggleButton("Reparaciones");
			tglbtnReparaciones.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					txtpnTexto.setText("<b>REPARACIONES</b>\r\n<br/><br/>\r\nLas reparaciones se realizan sobre un barco aliado que tenga alguna de sus partes dañadas. No se pueden realizar reparaciones sobre barcos sin partes dañadas, así como tampoco sobre un barco que tenga todas ellas dañadas, y por tanto esté destruido.\r\n<br/>\r\nLa reparación hace que sus casillas dañadas dejen de estarlo, y tiene un precio directamente proporcional al número de casillas reparadas: 60 de oro por cada una.\r\n");
					txtpnTexto.setCaretPosition(0);
				}
			});
			buttonGroup.add(tglbtnReparaciones);
			tglbtnReparaciones.setBounds(10, 182, 115, 23);
		}
		return tglbtnReparaciones;
	}
	
	private JToggleButton getTglbtnIntroduccin() {
		if (tglbtnIntroduccin == null) {
			tglbtnIntroduccin = new JToggleButton("Introducción");
			tglbtnIntroduccin.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					txtpnTexto.setText("<b>HUNDIR LA FLOTA</b><br/><br/>\r\nHundir la flota es un juego ambientado en un combate naval en el que dos personas luchan por la victoria en el campo de batalla. El objetivo del juego es destruir todos los arcos del rival antes de que este haya destruido los nuestros, ayudados por una gran cantidad de armas y herramientas diferentes.\r\n<br/><br/>\r\nPara encontrar más información acerca del juego, puede utilizar los botones de la izquierda de la pantalla.");
					txtpnTexto.setCaretPosition(0);
				}
			});
			buttonGroup.add(tglbtnIntroduccin);
			tglbtnIntroduccin.setBounds(10, 12, 115, 23);
		}
		return tglbtnIntroduccin;
	}
	
	private JToggleButton getTglbtnTableros() {
		if (tglbtnTableros == null) {
			tglbtnTableros = new JToggleButton("Tableros");
			tglbtnTableros.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					txtpnTexto.setText("<b>TABLEROS</b>\r\n<br/><br/>\r\nEl jugador es presentado con dos tableros:\r\n<br/>\r\nEl de la izquierda es en el que se colocarán los barcos de su flota. En él, el jugador podrá ver sus barcos y dar las órdenes de repararlos o escudarlos y también se reflejarán los resultados de los disparos del oponente.\r\n<br/>\r\nEl de la derecha es en el que se encuentran los barcos del oponente. Este tablero comenzará oculto: no se podrá ver el contenido de las casillas. Mediante disparos con las distintas armas el tablero irá revelándose al mismo tiempo que se destruyen los barcos enemigos. Todas las casillas que sean alcanzadas revelarán su contenido.\r\n");
					txtpnTexto.setCaretPosition(0);
				}
			});
			buttonGroup.add(tglbtnTableros);
			tglbtnTableros.setBounds(10, 80, 115, 23);
		}
		return tglbtnTableros;
	}
	
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBounds(135, 12, 339, 226);
			scrollPane.setViewportView(getTxtpnTexto());
		}
		return scrollPane;
	}
	
	private JTextPane getTxtpnTexto() {
		if (txtpnTexto == null) {
			txtpnTexto = new JTextPane();
			txtpnTexto.setBackground(new Color(255, 228, 196));
			txtpnTexto.setEditable(false);
			txtpnTexto.setContentType("text/html");
			txtpnTexto.setText("<b>HUNDIR LA FLOTA</b><br/><br/>\r\nHundir la flota es un juego ambientado en un combate naval en el que dos personas luchan por la victoria en el campo de batalla. El objetivo del juego es destruir todos los arcos del rival antes de que este haya destruido los nuestros, ayudados por una gran cantidad de armas y herramientas diferentes.\r\n<br/><br/>\r\nPara encontrar más información acerca del juego, puede utilizar los botones de la izquierda de la pantalla.");
			}
		return txtpnTexto;
	}
	
	private JToggleButton getTglbtnEscudos() {
		if (tglbtnEscudos == null) {
			tglbtnEscudos = new JToggleButton("Escudos");
			buttonGroup.add(tglbtnEscudos);
			tglbtnEscudos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					txtpnTexto.setText("<b>ESCUDOS</b>\r\n<br/><br/>\r\nUn escudo proporciona protección a un barco aliado, anulando una serie de disparos que se realice sobre ellos. El escudo es global en relación al barco, y cualquier disparo sobre este último afecta de igual manera al escudo. Esto quiere decir que cuando se destruye el escudo al disparar a una casilla, todo el barco pierde el escudo.\r\n<br/>\r\nUn escudo es capaz de resistir dos disparos de bombas antes de ser destruidos, y un disparo de cualquier tipo de misil.\r\n<br/>\r\nLos escudos se colocan sobre cualquier barco no destruido de la flota aliada. Su coste es directamente proporcional al tamaño del barco que se quiere escudar: 40 de oro por cada casilla que ocupa el barco.\r\n");
					txtpnTexto.setCaretPosition(0);
				}
			});
			tglbtnEscudos.setBounds(10, 216, 115, 23);
		}
		return tglbtnEscudos;
	}
	
}
