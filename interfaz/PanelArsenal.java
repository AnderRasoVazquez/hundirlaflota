package interfaz;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Observable;
import java.util.Observer;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import codigo.Arsenal;
import codigo.Juego;
import codigo.excepciones.NotEnoughCashException;

@SuppressWarnings("serial")
public class PanelArsenal extends JPanel implements Observer {

    Arsenal arsenal;
    private JButton btnBombas;
    private JRadioButton rdbtnBombas;
    private final ButtonGroup rdbtnsArmas = new ButtonGroup();
    private JTextField txtfBombas;
    private JButton btnMisilNS;
    private JRadioButton rdbtnMisilNS;
    private JTextField txtfMisilNS;
    private JButton btnMisil;
    private JRadioButton rdbtnMisil;
    private JTextField txtfMisil;
    private JButton btnMisilEO;
    private JRadioButton rdbtnMisilEO;
    private JTextField txtfMisilEO;
    private JButton btnRadar;
    private JRadioButton rdbtnRadar;
    private JTextField txtfRadar;
    private JButton btnMisilBoom;
    private JRadioButton rdbtnMisilBoom;
    private JTextField txtfMisilBoom;
    private JLabel lblOro;
    private JLabel lblPMisil;
    private JLabel lblPMisilNS;
    private JLabel lblPMisilEO;
    private JLabel lblPMisilBoom;
    private JLabel lblPEscudo;
    private JLabel lblPReparacion;
    private JToggleButton tglbtnReparacion;
    private JToggleButton tglbtnEscudo;
    private final ButtonGroup tglbtnsRepEsc = new ButtonGroup();

    public PanelArsenal(int pX, int pY, int pWidth, int pHeight, Arsenal pArsenal) {
        setBorder(new LineBorder(new Color(0, 0, 0), 1));
        setBounds(pX, pY, pWidth, pHeight);
        setLayout(null);
        add(getBtnBombas());
        add(getBtnRadar());
        add(getBtnMisil());
        add(getBtnMisilNS());
        add(getBtnMisilEO());
        add(getBtnMisilBoom());
        add(getRdbtnBombas());
        add(getRdbtnRadar());
        add(getRdbtnMisil());
        add(getRdbtnMisilNS());
        add(getRdbtnMisilEO());
        add(getRdbtnMisilBoom());
        add(getTxtfBombas());
        add(getTxtfRadar());
        add(getTxtfMisil());
        add(getTxtfMisilNS());
        add(getTxtfMisilBoom());
        add(getTxtfMisilEO());
        add(getLblOro());
        add(getLblPMisil());
        add(getLblPMisilNS());
        add(getLblPMisilEO());
        add(getLblPMisilBoom());
        add(getLblPEscudo());
        add(getLblPReparacion());
        add(getTglbtnEscudo());
        add(getTglbtnReparacion());
        getRdbtnBombas().setSelected(true);
        arsenal = pArsenal;

        Juego.getElJuego().getJ1().getArsenal().addObserver(this);
    }

    /*
     * A continuación se encuentran los botones que permiten comprar munición de las armas, escudos y
     * reparaciones.
     */
    private JButton getBtnBombas() {
        if (btnBombas == null) {
            btnBombas = new JButton("");
            btnBombas.setToolTipText("Dispones de bombas ilimitadas, por lo que no se pueden comprar más.");
            btnBombas.setBounds(34, 67, 40, 40);
            btnBombas.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    VentanaJuego.infoBox("Dispones de bombas ilimitadas, no es posible comprar más",
                            "Reabastecer Bombas");
                }
            });
            btnBombas.addMouseListener(new MouseAdapter() {
            	public void mouseEntered(MouseEvent arg0) {
            		setCursor(VentanaJuego.cursorMano);
            	}
            	public void mouseExited(MouseEvent arg0){
            		setCursor(VentanaJuego.cursorNormal);
            	}
            });
            btnBombas.setIcon(new ImageIcon(PanelArsenal.class.getResource("/recursos/IBomba.png")));
        }
        return btnBombas;
    }

    private JButton getBtnRadar() {
        if (btnRadar == null) {
            btnRadar = new JButton("");
            btnRadar.setToolTipText("Dispones de radares limitados en la partida. No es posible comprar más.");
            btnRadar.setBounds(34, 131, 40, 40);
            btnRadar.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    VentanaJuego.infoBox("No se pueden comprar más usos de Radar",
                            "Reabastecer Radar");
                }
            });
            btnRadar.addMouseListener(new MouseAdapter() {
            	public void mouseEntered(MouseEvent arg0) {
            		setCursor(VentanaJuego.cursorMano);
            	}
            	public void mouseExited(MouseEvent arg0){
            		setCursor(VentanaJuego.cursorNormal);
            	}
            });
            btnRadar.setIcon(new ImageIcon(VentanaJuego.class.getResource("/recursos/IRadar.png")));
        }
        return btnRadar;
    }

    private JButton getBtnMisil() {
        if (btnMisil == null) {
            btnMisil = new JButton("");
            btnMisil.setToolTipText("Comprar misil.");
            btnMisil.setBounds(141, 67, 40, 40);
            btnMisil.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    String input = VentanaJuego.inputBox("¿Cuántos Misiles quieres comprar? ("
                                                             + arsenal.getArma(1).getPrecio()
                                                             + " oro por Misil)",
                            "Reabastecer Misiles");
                    if (input != null) {
                        try {
                            int cantidad = Integer.parseInt(input);
                            if (arsenal.reabastecer(1, cantidad)) {
                                VentanaJuego.infoBox("Misil(es) comprado(s)",
                                        "Reabastecer Misiles");
                            } else {
                                VentanaJuego.infoBox("Cantidad no válida",
                                        "Reabastecer Misiles");
                            }
                        } catch (NumberFormatException ex) {
                            VentanaJuego.infoBox("Cantidad no válida",
                                    "Reabastecer Misiles");
                        } catch (NotEnoughCashException ex) {
                            VentanaJuego.infoBox("No tienes suficiente dinero",
                                    "Reabastecer Misiles");
                        }
                    }
                }
            });
            btnMisil.addMouseListener(new MouseAdapter() {
            	public void mouseEntered(MouseEvent arg0) {
            		setCursor(VentanaJuego.cursorMano);
            	}
            	public void mouseExited(MouseEvent arg0){
            		setCursor(VentanaJuego.cursorNormal);
            	}
            });
            btnMisil.setIcon(new ImageIcon(VentanaJuego.class.getResource("/recursos/IMisil+.png")));
        }
        return btnMisil;
    }

    private JButton getBtnMisilNS() {
        if (btnMisilNS == null) {
            btnMisilNS = new JButton("");
            btnMisilNS.setToolTipText("Comprar misiles NS.");
            btnMisilNS.setBounds(248, 67, 40, 40);
            btnMisilNS.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    String input = VentanaJuego.inputBox("¿Cuántos MisilesNS quieres comprar? ("
                                                             + arsenal.getArma(2).getPrecio()
                                                             + " oro por MisilNS)",
                            "Reabastecer MisilesNS");
                    if (input != null) {
                        try {
                            int cantidad = Integer.parseInt(input);
                            if (arsenal.reabastecer(2, cantidad)) {
                                VentanaJuego.infoBox("Misil(es)NS comprado(s)",
                                        "Reabastecer MisilesNS");
                            } else {
                                VentanaJuego.infoBox("Cantidad no válida",
                                        "Reabastecer MisilesNS");
                            }
                        } catch (NumberFormatException ex) {
                            VentanaJuego.infoBox("Cantidad no válida",
                                    "Reabastecer MisilesNS");
                        } catch (NotEnoughCashException ex) {
                            VentanaJuego.infoBox("No tienes suficiente dinero",
                                    "Reabastecer MisilesNS");
                        }
                    }
                }
            });
            btnMisilNS.addMouseListener(new MouseAdapter() {
            	public void mouseEntered(MouseEvent arg0) {
            		setCursor(VentanaJuego.cursorMano);
            	}
            	public void mouseExited(MouseEvent arg0){
            		setCursor(VentanaJuego.cursorNormal);
            	}
            });
            btnMisilNS.setIcon(new ImageIcon(VentanaJuego.class.getResource("/recursos/IMisilNS+.png")));
        }
        return btnMisilNS;
    }

    private JButton getBtnMisilEO() {
        if (btnMisilEO == null) {
            btnMisilEO = new JButton("");
            btnMisilEO.setBounds(141, 131, 40, 40);
            btnMisilEO.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    String input = VentanaJuego.inputBox("¿Cuántos MisilesEO quieres comprar? ("
                                                             + arsenal.getArma(3).getPrecio()
                                                             + " oro por MisilEO)",
                            "Reabastecer MisilesEO");
                    if (input != null) {
                        try {
                            int cantidad = Integer.parseInt(input);
                            if (arsenal.reabastecer(3, cantidad)) {
                                VentanaJuego.infoBox("Misil(es)EO comprado(s)",
                                        "Reabastecer MisilesEO");
                            } else {
                                VentanaJuego.infoBox("Cantidad no válida",
                                        "Reabastecer MisilesEO");
                            }
                        } catch (NumberFormatException ex) {
                            VentanaJuego.infoBox("Cantidad no válida",
                                    "Reabastecer MisilesEO");
                        } catch (NotEnoughCashException ex) {
                            VentanaJuego.infoBox("No tienes suficiente dinero",
                                    "Reabastecer MisilesEO");
                        }
                    }
                }
            });
            btnMisilEO.addMouseListener(new MouseAdapter() {
            	public void mouseEntered(MouseEvent arg0) {
            		setCursor(VentanaJuego.cursorMano);
            	}
            	public void mouseExited(MouseEvent arg0){
            		setCursor(VentanaJuego.cursorNormal);
            	}
            });
            btnMisilEO.setToolTipText("Comprar misiles EO.");
            btnMisilEO.setIcon(new ImageIcon(VentanaJuego.class.getResource("/recursos/IMisilEO+.png")));
        }
        return btnMisilEO;
    }

    private JButton getBtnMisilBoom() {
        if (btnMisilBoom == null) {
            btnMisilBoom = new JButton("");
            btnMisilBoom.setToolTipText("Comprar misiles BOOM.");
            btnMisilBoom.setBounds(248, 131, 40, 40);
            btnMisilBoom.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    String input = VentanaJuego.inputBox("¿Cuántos MisilesBOOM quieres comprar? ("
                                                             + arsenal.getArma(4).getPrecio()
                                                             + " oro por MisilBOOM)",
                            "Reabastecer MisilesBOOM");
                    if (input != null) {
                        try {
                            int cantidad = Integer.parseInt(input);
                            if (arsenal.reabastecer(4, cantidad)) {
                                VentanaJuego.infoBox("Misil(es)BOOM comprado(s)",
                                        "Reabastecer MisilesBOOM");
                            } else {
                                VentanaJuego.infoBox("Cantidad no válida",
                                        "Reabastecer MisilesBOOM");
                            }
                        } catch (NumberFormatException ex) {
                            VentanaJuego.infoBox("Cantidad no válida",
                                    "Reabastecer MisilesBOOM");
                        } catch (NotEnoughCashException ex) {
                            VentanaJuego.infoBox("No tienes suficiente dinero",
                                    "Reabastecer MisilesBOOM");
                        }
                    }
                }
            });
            btnMisilBoom.addMouseListener(new MouseAdapter() {
            	public void mouseEntered(MouseEvent arg0) {
            		setCursor(VentanaJuego.cursorMano);
            	}
            	public void mouseExited(MouseEvent arg0){
            		setCursor(VentanaJuego.cursorNormal);
            	}
            });
            btnMisilBoom.setIcon(new ImageIcon(VentanaJuego.class.getResource("/recursos/IExplosion+.png")));
        }
        return btnMisilBoom;
    }

    /*
     * A continuación se encuentran los RadioButton que permiten seleccionar el arma que va a ser utilizada.
     */
    private JRadioButton getRdbtnBombas() {
        if (rdbtnBombas == null) {
            rdbtnBombas = new JRadioButton("");
            rdbtnBombas.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    if (rdbtnBombas.isSelected()) {
                        Juego.getElJuego().getJ1().equiparArma(0);
                    }
                }
            });
            rdbtnBombas.setBounds(14, 77, 20, 20);
            rdbtnsArmas.add(rdbtnBombas);
        }
        return rdbtnBombas;
    }

    private JRadioButton getRdbtnRadar() {
        if (rdbtnRadar == null) {
            rdbtnRadar = new JRadioButton("");
            rdbtnRadar.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    if (rdbtnRadar.isSelected()) {
                        Juego.getElJuego().getJ1().equiparArma(5);
                    }
                }
            });
            rdbtnRadar.setBounds(14, 141, 20, 20);
            rdbtnsArmas.add(rdbtnRadar);
        }
        return rdbtnRadar;
    }

    private JRadioButton getRdbtnMisil() {
        if (rdbtnMisil == null) {
            rdbtnMisil = new JRadioButton("");
            rdbtnMisil.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    if (rdbtnMisil.isSelected()) {
                        Juego.getElJuego().getJ1().equiparArma(1);
                    }
                }
            });
            rdbtnMisil.setBounds(121, 77, 20, 20);
            rdbtnsArmas.add(rdbtnMisil);
        }
        return rdbtnMisil;
    }

    private JRadioButton getRdbtnMisilNS() {
        if (rdbtnMisilNS == null) {
            rdbtnMisilNS = new JRadioButton("");
            rdbtnMisilNS.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    if (rdbtnMisilNS.isSelected()) {
                        Juego.getElJuego().getJ1().equiparArma(2);
                    }
                }
            });
            rdbtnMisilNS.setBounds(228, 77, 20, 20);
            rdbtnsArmas.add(rdbtnMisilNS);
        }
        return rdbtnMisilNS;
    }

    private JRadioButton getRdbtnMisilEO() {
        if (rdbtnMisilEO == null) {
            rdbtnMisilEO = new JRadioButton("");
            rdbtnMisilEO.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    if (rdbtnMisilEO.isSelected()) {
                        Juego.getElJuego().getJ1().equiparArma(3);
                    }
                }
            });
            rdbtnMisilEO.setBounds(121, 141, 20, 20);
            rdbtnsArmas.add(rdbtnMisilEO);
        }
        return rdbtnMisilEO;
    }

    private JRadioButton getRdbtnMisilBoom() {
        if (rdbtnMisilBoom == null) {
            rdbtnMisilBoom = new JRadioButton("");
            rdbtnMisilBoom.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    if (rdbtnMisilBoom.isSelected()) {
                        Juego.getElJuego().getJ1().equiparArma(4);
                    }
                }
            });
            rdbtnMisilBoom.setBounds(228, 141, 20, 20);
            rdbtnsArmas.add(rdbtnMisilBoom);
        }
        return rdbtnMisilBoom;
    }

    /*
     * A continuación se encuentran los campos de texto que muestran la munición de cada una de nuestras
     * armas.
     */
    private JTextField getTxtfBombas() {
        if (txtfBombas == null) {
            txtfBombas = new JTextField();
            txtfBombas.setBounds(73, 68, 38, 38);
            txtfBombas.setHorizontalAlignment(SwingConstants.CENTER);
            txtfBombas.setBackground(Color.WHITE);
            txtfBombas.setEditable(false);
            txtfBombas.setText("∞");
            txtfBombas.setColumns(10);
        }
        return txtfBombas;
    }

    private JTextField getTxtfRadar() {
        if (txtfRadar == null) {
            txtfRadar = new JTextField();
            txtfRadar.setBounds(73, 132, 38, 38);
            txtfRadar.setHorizontalAlignment(SwingConstants.CENTER);
            txtfRadar.setText(((Integer) Juego.getElJuego().getJ1().getArsenal().getArma(5).getMunicion())
                                      .toString());
            txtfRadar.setEditable(false);
            txtfRadar.setColumns(10);
            txtfRadar.setBackground(Color.WHITE);
        }
        return txtfRadar;
    }

    private JTextField getTxtfMisil() {
        if (txtfMisil == null) {
            txtfMisil = new JTextField();
            txtfMisil.setBounds(180, 68, 38, 38);
            txtfMisil.setHorizontalAlignment(SwingConstants.CENTER);
            txtfMisil.setText(((Integer) Juego.getElJuego().getJ1().getArsenal().getArma(1).getMunicion())
                                      .toString());
            txtfMisil.setEditable(false);
            txtfMisil.setColumns(10);
            txtfMisil.setBackground(Color.WHITE);
        }
        return txtfMisil;
    }

    private JTextField getTxtfMisilNS() {
        if (txtfMisilNS == null) {
            txtfMisilNS = new JTextField();
            txtfMisilNS.setBounds(287, 68, 38, 38);
            txtfMisilNS.setHorizontalAlignment(SwingConstants.CENTER);
            txtfMisilNS.setText(((Integer) Juego.getElJuego().getJ1().getArsenal().getArma(2).getMunicion())
                                        .toString());
            txtfMisilNS.setEditable(false);
            txtfMisilNS.setColumns(10);
            txtfMisilNS.setBackground(Color.WHITE);
        }
        return txtfMisilNS;
    }

    private JTextField getTxtfMisilEO() {
        if (txtfMisilEO == null) {
            txtfMisilEO = new JTextField();
            txtfMisilEO.setBounds(180, 132, 38, 38);
            txtfMisilEO.setHorizontalAlignment(SwingConstants.CENTER);
            txtfMisilEO.setText(((Integer) Juego.getElJuego().getJ1().getArsenal().getArma(3).getMunicion())
                                        .toString());
            txtfMisilEO.setEditable(false);
            txtfMisilEO.setColumns(10);
            txtfMisilEO.setBackground(Color.WHITE);
        }
        return txtfMisilEO;
    }

    private JTextField getTxtfMisilBoom() {
        if (txtfMisilBoom == null) {
            txtfMisilBoom = new JTextField();
            txtfMisilBoom.setBounds(287, 132, 38, 38);
            txtfMisilBoom.setHorizontalAlignment(SwingConstants.CENTER);
            txtfMisilBoom
                    .setText(((Integer) Juego.getElJuego().getJ1().getArsenal().getArma(4).getMunicion())
                                     .toString());
            txtfMisilBoom.setEditable(false);
            txtfMisilBoom.setColumns(10);
            txtfMisilBoom.setBackground(Color.WHITE);
        }
        return txtfMisilBoom;
    }

    /*
     * A continuación se encuentran las etiquetas que muestra el oro disponible y el precio de las distintas
     * armas.
     */
    private JLabel getLblOro() {
        if (lblOro == null) {
            lblOro = new JLabel("<html><b><font size=+1>"
                                        + ((Integer) Juego.getElJuego().getJ1().getArsenal().getDinero()).toString()
                                        + "</font></b></html>");
            lblOro.setBounds(359, 11, 99, 40);
            lblOro.setHorizontalAlignment(SwingConstants.CENTER);
            lblOro.setIcon(new ImageIcon(VentanaJuego.class.getResource("/recursos/IMonedas.png")));
        }
        return lblOro;
    }

    private JLabel getLblPMisil() {
        if (lblPMisil == null) {
            lblPMisil = new JLabel(
                                          ((Integer) Juego.getElJuego().getJ1().getArsenal().getArma(1).getPrecio()).toString());
            lblPMisil.setBounds(163, 47, 55, 20);
            lblPMisil.setIcon(new ImageIcon(VentanaJuego.class.getResource("/recursos/IMoneda.png")));
        }
        return lblPMisil;
    }

    private JLabel getLblPMisilNS() {
        if (lblPMisilNS == null) {
            lblPMisilNS = new JLabel(((Integer) Juego.getElJuego().getJ1().getArsenal().getArma(2)
                                                        .getPrecio()).toString());
            lblPMisilNS.setBounds(270, 47, 55, 20);
            lblPMisilNS.setIcon(new ImageIcon(VentanaJuego.class.getResource("/recursos/IMoneda.png")));
        }
        return lblPMisilNS;
    }

    private JLabel getLblPMisilEO() {
        if (lblPMisilEO == null) {
            lblPMisilEO = new JLabel(((Integer) Juego.getElJuego().getJ1().getArsenal().getArma(3)
                                                        .getPrecio()).toString());
            lblPMisilEO.setBounds(163, 111, 55, 20);
            lblPMisilEO.setIcon(new ImageIcon(VentanaJuego.class.getResource("/recursos/IMoneda.png")));
        }
        return lblPMisilEO;
    }

    private JLabel getLblPMisilBoom() {
        if (lblPMisilBoom == null) {
            lblPMisilBoom = new JLabel(((Integer) Juego.getElJuego().getJ1().getArsenal().getArma(4)
                                                          .getPrecio()).toString());
            lblPMisilBoom.setBounds(270, 111, 55, 20);
            lblPMisilBoom.setIcon(new ImageIcon(VentanaJuego.class.getResource("/recursos/IMoneda.png")));
        }
        return lblPMisilBoom;
    }

    private JLabel getLblPEscudo() {
        if (lblPEscudo == null) {
            lblPEscudo = new JLabel(((Integer) Arsenal.PRECIO_ESCUDO).toString() + "x");
            lblPEscudo.setIcon(new ImageIcon(PanelArsenal.class.getResource("/recursos/IMoneda.png")));
            lblPEscudo.setBounds(403, 75, 55, 20);
            lblPEscudo.setToolTipText("El precio se multiplica por cada parte de barco escudada.");
        }
        return lblPEscudo;
    }

    private JLabel getLblPReparacion() {
        if (lblPReparacion == null) {
            lblPReparacion = new JLabel(((Integer) Arsenal.PRECIO_REPARACION).toString() + "x");
            lblPReparacion.setIcon(new ImageIcon(PanelArsenal.class.getResource("/recursos/IMoneda.png")));
            lblPReparacion.setBounds(403, 141, 55, 20);
            lblPReparacion.setToolTipText("El precio se multiplica por cada parte de barco reparada.");
        }
        return lblPReparacion;
    }

    public void refrescarTextos() {
        getTxtfMisil().setText(Integer.toString(arsenal.getArma(1).getMunicion()));
        getTxtfMisilNS().setText(Integer.toString(arsenal.getArma(2).getMunicion()));
        getTxtfMisilEO().setText(Integer.toString(arsenal.getArma(3).getMunicion()));
        getTxtfMisilBoom().setText(Integer.toString(arsenal.getArma(4).getMunicion()));
        getTxtfRadar().setText(Integer.toString(arsenal.getArma(5).getMunicion()));
        getLblOro().setText("<html><b><font size=+1>" +
                                    Integer.toString(arsenal.getDinero()) +
                                    "</font></b></html>");
    }

    private JToggleButton getTglbtnReparacion() {
        if (tglbtnReparacion == null) {
            tglbtnReparacion = new JToggleButton("");
            tglbtnReparacion.setIcon(new ImageIcon(PanelArsenal.class.getResource("/recursos/IReparaciones.png")));
            tglbtnsRepEsc.add(tglbtnReparacion);
            tglbtnReparacion.setBounds(353, 131, 40, 40);
	        tglbtnReparacion.addMouseListener(new MouseAdapter() {
	            public void mouseClicked(MouseEvent mouseEvent) {
	                setCursor(VentanaJuego.cursorNormal);
	            }
	        	public void mouseEntered(MouseEvent arg0) {
	        		if (!getEstadoReparacion()) {
	        			setCursor(VentanaJuego.cursorMano);
	        		}
	        	}
	        	public void mouseExited(MouseEvent arg0){
	        		setCursor(VentanaJuego.cursorNormal);
	        	}
	        });
        }
        return tglbtnReparacion;
    }

    private JToggleButton getTglbtnEscudo() {
        if (tglbtnEscudo == null) {
            tglbtnEscudo = new JToggleButton("");
            tglbtnEscudo.setIcon(new ImageIcon(PanelArsenal.class.getResource("/recursos/IEscudo.png")));
            tglbtnsRepEsc.add(tglbtnEscudo);
            tglbtnEscudo.setBounds(353, 67, 40, 40);
	        tglbtnEscudo.addMouseListener(new MouseAdapter() {
	            @Override
	            public void mouseClicked(MouseEvent mouseEvent) {
	                setCursor(VentanaJuego.cursorNormal);
	            }
	        	public void mouseEntered(MouseEvent arg0) {
	        		if (!getEstadoEscudo()) {
	        			setCursor(VentanaJuego.cursorMano);
	        		}
	        	}
	        	public void mouseExited(MouseEvent arg0){
	        		setCursor(VentanaJuego.cursorNormal);
	        	}
	        });
        }
        return tglbtnEscudo;
    }

    public boolean getEstadoReparacion() {
        return getTglbtnReparacion().isSelected();
    }

    public boolean getEstadoEscudo() {
        return getTglbtnEscudo().isSelected();
    }

    public void desactivarBotones() {
        tglbtnsRepEsc.clearSelection();
    }

    @Override
    public void update(Observable arg0, Object arg1) {
        // observa a Arsenal del jugador
        refrescarTextos();
    }
}
