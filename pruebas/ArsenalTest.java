package pruebas;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import codigo.Arsenal;
import codigo.Casilla;
import codigo.Flota;
import codigo.excepciones.NotEnoughCashException;

public class ArsenalTest {

    Arsenal arsenal;
    Flota flota;

    private Arsenal getArsenal() {
        if (arsenal == null) {
            arsenal = new Arsenal();
        }
        return arsenal;
    }

    private Flota getFlota() {
        if (flota == null) {
            flota = new Flota();
            flota.colocacionControlada();
        }
        return flota;
    }

    @Test
    public void testEquiparArma() {
        getArsenal().equiparArma(0);
        assertEquals("bombas", getArsenal().getArmaEquipada().getNombre().toLowerCase());
        getArsenal().equiparArma(1);
        assertEquals("misiles", getArsenal().getArmaEquipada().getNombre().toLowerCase());
        getArsenal().equiparArma(2);
        assertEquals("misiles norte-sur", getArsenal().getArmaEquipada().getNombre().toLowerCase());
        getArsenal().equiparArma(3);
        assertEquals("misiles este-oeste", getArsenal().getArmaEquipada().getNombre().toLowerCase());
        getArsenal().equiparArma(4);
        assertEquals("misiles boom", getArsenal().getArmaEquipada().getNombre().toLowerCase());
        getArsenal().equiparArma(5);
        assertEquals("el radar", getArsenal().getArmaEquipada().getNombre().toLowerCase());
    }

    @Test
    public void testDisparar() {
        Casilla casilla = new Casilla(0, 0);
        getArsenal().equiparArma(1);
        getArsenal().disparar(getFlota(), casilla);
        assertEquals("danado", getFlota().getContenido(casilla).toString().toLowerCase());
    }

    @Test
    public void testReparar() {
        Casilla casilla = new Casilla(0, 0);
        getArsenal().disparar(getFlota(), casilla);
        getArsenal().repararBarco(getFlota(), casilla);
        assertEquals("barco", getFlota().getContenido(casilla).toString().toLowerCase());
    }

    @Test
    public void testActivarEscudo() {
        Casilla casilla = new Casilla(0, 0);
        getArsenal().activarEscudo(getFlota(), casilla);
        assertEquals("escudo", getFlota().getContenido(casilla).toString().toLowerCase());
    }

    @Test
    public void testReabastecer() {
        getArsenal().reabastecer(1, 1);
        assertEquals(4, getArsenal().getArma(1).getMunicion());
        getArsenal().reabastecer(1, -1);
        assertEquals(4, getArsenal().getArma(1).getMunicion());
        try {
            getArsenal().reabastecer(1, 45);
            assertEquals(4, getArsenal().getArma(1).getMunicion());
        } catch (NotEnoughCashException e) {
            assertEquals(4, getArsenal().getArma(1).getMunicion());
        }
        getArsenal().reabastecer(0, 1);
        assertEquals(1, getArsenal().getArma(0).getMunicion());
        getArsenal().reabastecer(5, 1);
        assertEquals(3, getArsenal().getArma(5).getMunicion());
    }

    @Test
    public void testGetArma() {
        assertEquals("bombas", getArsenal().getArma(0).getNombre().toLowerCase());
        assertEquals("misiles", getArsenal().getArma(1).getNombre().toLowerCase());
        assertEquals("misiles norte-sur", getArsenal().getArma(2).getNombre().toLowerCase());
        assertEquals("misiles este-oeste", getArsenal().getArma(3).getNombre().toLowerCase());
        assertEquals("misiles boom", getArsenal().getArma(4).getNombre().toLowerCase());
        assertEquals("el radar", getArsenal().getArma(5).getNombre().toLowerCase());
    }

}
