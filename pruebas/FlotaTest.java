package pruebas;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;

import codigo.Casilla;
import codigo.Flota;
import codigo.Orientacion;

public class FlotaTest {

    Flota flota;

    private Flota getFlota() {
        if (flota == null) {
            flota = new Flota();
            flota.colocacionControlada();
        }
        return flota;
    }

    /**
     * Test para comprobar que el método Flota.barcoEnCasilla() funciona correctamente.
     */
    @Test
    public void testBarcoEnCasilla() {
        // casilla con portaaviones
        assertEquals(4, getFlota().barcoEnCasilla(new Casilla(0, 0)).getNumSegmentos());
        // casilla vacía
        assertNull(getFlota().barcoEnCasilla(new Casilla(4, 5)));
        // casilla OoB
        assertNull(getFlota().barcoEnCasilla(new Casilla(6, 10)));
    }
    /**
     * Test para comprobar que el método Flota.barcosEnCasillas() funciona correctamente.
     */
    @Test
    public void testBarcosEnCasillas() {
        Collection<Casilla> casillas;
        // fila 0, con cuatro barcos
        casillas = Flota.encontrarCasillas(new Casilla(0, 0), Orientacion.ESTE, 10);
        assertEquals(4, getFlota().barcosEnCasillas(casillas).size());
        // columna 0, con tres barcos
        casillas.clear();
        casillas = Flota.encontrarCasillas(new Casilla(0, 0), Orientacion.SUR, 10);
        assertEquals(3, getFlota().barcosEnCasillas(casillas).size());
        // fila 5, sin barcos
        casillas.clear();
        casillas = Flota.encontrarCasillas(new Casilla(5, 0), Orientacion.ESTE, 10);
        assertEquals(0, getFlota().barcosEnCasillas(casillas).size());
        // casillas OoB
        casillas.clear();
        casillas.add(new Casilla(-1, 11));
        casillas.add(new Casilla(-2, 10));
        casillas.add(new Casilla(-3, 9));
        assertEquals(0, getFlota().barcosEnCasillas(casillas).size());
        // casillas null
        assertEquals(0, getFlota().barcosEnCasillas(null).size());
    }

    /**
     * Test para comprobar que el método Flota.casillaColocable() funciona correctamente.
     */
    @Test
    public void testCasillaColocable() {
        // casilla colocable
        assertTrue(getFlota().casillaColocable(new Casilla(6, 7)));
        // casilla ocupada
        assertFalse(getFlota().casillaColocable(new Casilla(0, 0)));
        // casilla adjacente a una ocupada
        assertFalse(getFlota().casillaColocable(new Casilla(0, 1)));
        // casilla diagonal a una ocupada
        assertFalse(getFlota().casillaColocable(new Casilla(4, 1)));
        // casillas OoB
        assertFalse(getFlota().casillaColocable(new Casilla(6, 10)));
    }

    /**
     * Test para comprobar que el método estático Flota.casillaEnTablero() funciona correctamente.
     */
    @Test
    public void testCasillaEnTablero() {
        // casilla en tablero
        assertTrue(Flota.casillaEnTablero(new Casilla(6, 7)));
        // casilla OoB
        assertFalse(Flota.casillaEnTablero(new Casilla(6, 10)));
    }

    /**
     * Test para comprobar que el método estático Flota.econtrarCasillas() funciona correctamente.
     */
    @Test
    public void testEcontrarCasillas() {
        Casilla casilla = new Casilla(3, 4);
        int longitud = 5;
        Orientacion orientacion = Orientacion.ESTE;
        Collection<Casilla> casillas = new ArrayList<Casilla>(5);
        casillas.add(new Casilla(3, 4));
        casillas.add(new Casilla(3, 5));
        casillas.add(new Casilla(3, 6));
        casillas.add(new Casilla(3, 7));
        casillas.add(new Casilla(3, 8));
        assertEquals(casillas, Flota.encontrarCasillas(casilla, orientacion, longitud));
    }
}
