package pruebas;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

import codigo.Casilla;
import codigo.Humano;
import codigo.Juego;
import codigo.cpu.CPU;

public class CPUTest {

    /**
     * Funcion para crear un objeto cpu con una generación de barcos hecha
     *
     * @return
     */
    private CPU getCPU() {
        CPU cpu = new CPU();
        boolean generacion_exitosa = cpu.colocarBarcos();
        int maximoIntentos = 100;
        int contadorIntentos = 0;
        while (!generacion_exitosa && contadorIntentos < maximoIntentos) {
            contadorIntentos++;
            cpu.recogerBarcos();
            generacion_exitosa = cpu.colocarBarcos();
        }
        return cpu;
    }

    /**
     * Dado un caracter cuenta sus apariciones en un string
     *
     * @param pChar
     * @param pString
     * @return
     */
    @SuppressWarnings("unused")
    private int countChars(char pChar, String pString) {
        int contador = 0;
        for (int i = 0; i < pString.length(); i++) {
            if (pString.charAt(i) == pChar) {
                contador++;
            }
        }
        return contador;
    }

    /**
     * test para comprobar que la generacion de barcos no da errores
     */
    @Test
    public void colocarBarcos() {
        CPU cpu = new CPU();

        // si ha fallado la generación intenta generarla de nuevo
        boolean generacion_exitosa = cpu.colocarBarcos();
        int maximoIntentos = 100;
        int contadorIntentos = 0;
        while (!generacion_exitosa && contadorIntentos < maximoIntentos) {
            contadorIntentos++;
            cpu.recogerBarcos();
            generacion_exitosa = cpu.colocarBarcos();
        }
        assertTrue(generacion_exitosa);
    }

    /**
     * comprobar que se han asignado bien las posiciones a los barcos
     */
    @SuppressWarnings("unused")
    @Test
    public void resultadoColocarBarcos() {
        // numero por su longitud
        int charPortaaviones = 1 * 4;
        int charSubmarinos = 2 * 3;
        int charDestructores = 3 * 2;
        int charFragatas = 4 * 1;

        CPU cpu = this.getCPU();

        String tablero = cpu.getFlota().returnTablero();
        System.out.println(tablero);
    }

    /**
     * Test para comprobar que pueden darse los dos casos de orientaciones
     */
    @Test
    public void getOrientacion() {
        CPU cpu = this.getCPU();
        boolean haSalidoTrue = false;
        boolean haSalidoFalse = false;
        for (int i = 0; i < 100; i++) {
            if (cpu.getOrientacionTest()) {
                haSalidoTrue = true;
            } else {
                haSalidoFalse = true;
            }
        }

        assertTrue(haSalidoTrue && haSalidoFalse);
    }

    /**
     * test para comparar que coger un punto al azar es un punto valido
     */
    @Test
    public void getPuntoAlAzar() {
        CPU cpu = this.getCPU();
        assertTrue(cpu.getPosicionesLibresTest().contains(cpu.getPuntoLibreAlAzarTest()));
    }

    /**
     * test para comprobar que eliminar las posiciones incompatibles se hace de
     * la forma correcta
     */
    @Test
    public void eliminarPuntosIncompatibles() {
        CPU cpu = new CPU();
        ArrayList<Casilla> puntosLibresCPU = cpu.getPosicionesLibresTest();
        ArrayList<Casilla> puntosLibresTest = new ArrayList<>(puntosLibresCPU);
        ArrayList<Casilla> puntosIncompatibles = new ArrayList<>();
        puntosIncompatibles.add(new Casilla(2, 2));
        puntosIncompatibles.add(new Casilla(2, 3));

        cpu.eliminarPuntosIncompatiblesTest(puntosIncompatibles);
        puntosLibresTest.remove(new Casilla(1, 1));
        puntosLibresTest.remove(new Casilla(1, 2));
        puntosLibresTest.remove(new Casilla(1, 3));
        puntosLibresTest.remove(new Casilla(1, 4));
        puntosLibresTest.remove(new Casilla(2, 1));
        puntosLibresTest.remove(new Casilla(2, 2));
        puntosLibresTest.remove(new Casilla(2, 3));
        puntosLibresTest.remove(new Casilla(2, 4));
        puntosLibresTest.remove(new Casilla(3, 1));
        puntosLibresTest.remove(new Casilla(3, 2));
        puntosLibresTest.remove(new Casilla(3, 3));
        puntosLibresTest.remove(new Casilla(3, 4));

        assertTrue(puntosLibresTest.equals(puntosLibresCPU));
    }

    /**
     * Test para comprobar que obtener puntos adyacentes da un array con los puntos correctos
     */
    @Test
    public void puntosAdyacentes() {
        CPU cpu = getCPU();
        Casilla punto = new Casilla(5, 5);
        ArrayList<Casilla> puntosAdyacentes = cpu.getPuntosAdyacentesTest(punto);
        ArrayList<Casilla> adyacentesTest = new ArrayList<>();

        adyacentesTest.add(new Casilla(4, 4));
        adyacentesTest.add(new Casilla(4, 5));
        adyacentesTest.add(new Casilla(4, 6));
        adyacentesTest.add(new Casilla(5, 4));
        adyacentesTest.add(new Casilla(5, 5));
        adyacentesTest.add(new Casilla(5, 6));
        adyacentesTest.add(new Casilla(6, 4));
        adyacentesTest.add(new Casilla(6, 5));
        adyacentesTest.add(new Casilla(6, 6));

        assertTrue(adyacentesTest.equals(puntosAdyacentes));
    }

    /**
     * Test para comprobar que se dispara al barco horizontal de forma correcta
     */
    @Test
    public void hundirBarcoHorizontal() {
        Juego juego = Juego.getElJuego();
        Humano enemigo = new Humano();

        ArrayList<Casilla> posicionesBarcoHumano = new ArrayList<>();
        posicionesBarcoHumano.add(new Casilla(1, 1));
        posicionesBarcoHumano.add(new Casilla(1, 2));
        posicionesBarcoHumano.add(new Casilla(1, 3));
        posicionesBarcoHumano.add(new Casilla(1, 4));
        enemigo.getFlota().colocarBarco(posicionesBarcoHumano);

        CPU cpu = getCPU();
        cpu.inicializar(enemigo.getFlota());
        cpu.equiparArma(0);
        juego.setJ1(enemigo);
        juego.setJ2(cpu);

        System.out.println(cpu.getFlotaOponente().returnTablero());
        System.out.println(cpu.getFlota().returnTablero());

        cpu.setPosicionPivote(new Casilla(1,2)); // nice
        cpu.getPosicionesDisparables().remove(new Casilla(1,2));
        cpu.setEstado(cpu.getEstadoHundiendoBarco());

        System.out.println("ESTADO INICIAL PARA BARCO HORIZONTAL");
        System.out.println(cpu.returnTablero());
        cpu.comenzarTurnoTest();
        cpu.comenzarTurnoTest();
        cpu.comenzarTurnoTest();
        cpu.comenzarTurnoTest();
        cpu.comenzarTurnoTest();
        cpu.comenzarTurnoTest();
        cpu.comenzarTurnoTest();
    }

    /**
     * Test para comprobar que se dispara al barco horizontal de forma correcta
     */
    @Test
    public void hundirBarcoVertical() {
        Juego juego = Juego.getElJuego();
        Humano enemigo = new Humano();

        ArrayList<Casilla> posicionesBarcoHumano = new ArrayList<>();
        posicionesBarcoHumano.add(new Casilla(1,1));
        posicionesBarcoHumano.add(new Casilla(2,1));
        posicionesBarcoHumano.add(new Casilla(3,1));
        posicionesBarcoHumano.add(new Casilla(4,1));
        enemigo.getFlota().colocarBarco(posicionesBarcoHumano);

        CPU cpu = getCPU();
        cpu.inicializar(enemigo.getFlota());
        cpu.equiparArma(0);
        juego.setJ1(enemigo);
        juego.setJ2(cpu);

        System.out.println(cpu.getFlotaOponente().returnTablero());
        System.out.println(cpu.getFlota().returnTablero());

        cpu.setPosicionPivote(new Casilla(1,1)); // nice
        cpu.getPosicionesDisparables().remove(new Casilla(1,1));
        cpu.setEstado(cpu.getEstadoHundiendoBarco());

        System.out.println("ESTADO INICIAL PARA BARCO VERTICAL");
        System.out.println(cpu.returnTablero());
        cpu.comenzarTurnoTest();
        cpu.comenzarTurnoTest();
        cpu.comenzarTurnoTest();
        cpu.comenzarTurnoTest();
        cpu.comenzarTurnoTest();
    }
}

