package pruebas;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;

import codigo.Casilla;
import codigo.barcos.Barco;
import codigo.barcos.Portaaviones;

public class BarcoTest {

    Barco portaaviones;

    private Barco getPortaaviones() {
        if (portaaviones == null) {
            portaaviones = new Portaaviones();
            Collection<Casilla> casillas = new ArrayList<Casilla>(4);
            casillas.add(new Casilla(0, 0));
            casillas.add(new Casilla(0, 1));
            casillas.add(new Casilla(0, 2));
            casillas.add(new Casilla(0, 3));
            portaaviones.colocar(casillas);
        }
        return portaaviones;
    }

    /**
     * Test para comprobar que el método Barco.recoger() funciona correctamente.
     */
    @Test
    public void testRecoger() {
        getPortaaviones().recoger();
        assertFalse(getPortaaviones().estaColocado());
    }

    /**
     * Test para comprobar que el método Barco.colocar() funciona correctamente.
     */
    @Test
    public void testColocar() {
        getPortaaviones().recoger();
        Collection<Casilla> casillas = new ArrayList<Casilla>(4);
        // casillas no válidas
        casillas.add(new Casilla(0, 0));
        casillas.add(new Casilla(0, 2));
        casillas.add(new Casilla(0, 3));
        getPortaaviones().colocar(casillas);
        assertFalse(getPortaaviones().estaColocado());
        // casillas validas
        getPortaaviones().recoger();
        casillas.clear();
        casillas.add(new Casilla(0, 0));
        casillas.add(new Casilla(0, 1));
        casillas.add(new Casilla(0, 2));
        casillas.add(new Casilla(0, 3));
        getPortaaviones().colocar(casillas);
        assertTrue(getPortaaviones().estaColocado());
    }

    /**
     * Test para comprobar que el método Barco.getCasillas() funciona correctamente.
     */
    @Test
    public void testGetCasillas() {
        Collection<Casilla> casillas = new ArrayList<Casilla>(4);
        casillas.add(new Casilla(0, 0));
        casillas.add(new Casilla(0, 1));
        casillas.add(new Casilla(0, 2));
        casillas.add(new Casilla(0, 3));
        assertEquals(casillas, getPortaaviones().getCasillas());
    }

    /**
     * Test para comprobar que el método Barco.recibirDisparo() funciona correctamente.
     */
    @Test
    public void testRecibirDisparo() {
        getPortaaviones().activarEscudo();
        getPortaaviones().recibirDisparo(1, new Casilla(0, 0));
        assertTrue(getPortaaviones().estaEscudado());
        getPortaaviones().recibirDisparo(1, new Casilla(0, 1));
        assertFalse(getPortaaviones().estaEscudado());
        assertFalse(getPortaaviones().estaDanado());
        getPortaaviones().recibirDisparo(1, new Casilla(0, 0));
        getPortaaviones().recibirDisparo(1, new Casilla(0, 1));
        assertEquals(2, getPortaaviones().segmetosDanados());
        assertTrue((getPortaaviones().estaDanado()));
        assertFalse((getPortaaviones().estaDestruido()));
    }

    /**
     * Test para comprobar que el método Barco.recibirImpacto() funciona correctamente.
     */
    @Test
    public void testRecibirImpacto() {
        getPortaaviones().activarEscudo();
        getPortaaviones().recibirImpacto();
        assertFalse(getPortaaviones().estaEscudado());
        assertFalse(getPortaaviones().estaDanado());
        getPortaaviones().recibirImpacto();
        assertEquals(4, getPortaaviones().segmetosDanados());
        assertTrue((getPortaaviones().estaDanado()));
        assertTrue((getPortaaviones().estaDestruido()));
    }

    /**
     * Test para comprobar que el método Barco.reparar() funciona correctamente.
     */
    @Test
    public void testReparar() {
        // reparar el barco dañado
        getPortaaviones().recibirDisparo(1, new Casilla(0, 0));
        getPortaaviones().reparar();
        assertEquals(0, getPortaaviones().segmetosDanados());
        assertFalse((getPortaaviones().estaDanado()));
        assertFalse((getPortaaviones().estaDestruido()));
        // reparar el barco destruido
        getPortaaviones().recibirImpacto();
        getPortaaviones().reparar();
        assertEquals(4, getPortaaviones().segmetosDanados());
        assertTrue((getPortaaviones().estaDanado()));
        assertTrue((getPortaaviones().estaDestruido()));
    }

}
