package codigo.excepciones;

@SuppressWarnings("serial")
public class NotEnoughCashException extends RuntimeException {

    public NotEnoughCashException() {
    }

    public NotEnoughCashException(String var1) {
        super(var1);
    }

    public NotEnoughCashException(String var1, Throwable var2) {
        super(var1, var2);
    }

    public NotEnoughCashException(Throwable var1) {
        super(var1);
    }

    protected NotEnoughCashException(String var1, Throwable var2, boolean var3, boolean var4) {
        super(var1, var2, var3, var4);
    }
}
