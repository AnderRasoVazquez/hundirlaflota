package codigo.excepciones;

@SuppressWarnings("serial")
public class ArmaNoEquipadaException extends RuntimeException {

    public ArmaNoEquipadaException() {
    }

    public ArmaNoEquipadaException(String var1) {
        super(var1);
    }

    public ArmaNoEquipadaException(String var1, Throwable var2) {
        super(var1, var2);
    }

    public ArmaNoEquipadaException(Throwable var1) {
        super(var1);
    }

    protected ArmaNoEquipadaException(String var1, Throwable var2, boolean var3, boolean var4) {
        super(var1, var2, var3, var4);
    }
}
