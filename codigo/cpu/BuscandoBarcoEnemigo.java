package codigo.cpu;

import codigo.Casilla;

/**
 * Este es un estado para la cpu en la que se va a encargar de disparar aleatoriamente hasta que acierte a un barco
 */
public class BuscandoBarcoEnemigo implements IEstadoCPU{

    private CPU cpu;

    /**
     * Constructora.
     * @param cpu
     */
    public BuscandoBarcoEnemigo(CPU cpu){ this.cpu = cpu; }

    /**
     * Dispara al azar hasta dar con un barco.
     */
    @Override
    public void actuar() {
        System.out.println("Estado: buscando barco enemigo");
        Casilla puntoAlcanzado = cpu.getPuntoDisparableAlAzar();
        cpu.equiparArma(0);
        cpu.atacar(puntoAlcanzado);
//        for (Casilla puntoAlcanzado: puntosAlcanzados) {
            if (cpu.getFlotaOponente().casillaConBarco(puntoAlcanzado)){
                if (!cpu.getFlotaOponente().barcoEnCasilla(puntoAlcanzado).estaEscudado()) {
                    cpu.getPosicionesDisparables().remove(puntoAlcanzado);
                }
                cpu.getPosicionesConBarcoEnemigo().add(puntoAlcanzado);
                System.out.println("Tocado: " + puntoAlcanzado.toString());
                cpu.setPosicionPivote(puntoAlcanzado);
                cpu.setEstado(cpu.getEstadoHundiendoBarco());
            } else {
                System.out.println("Agua: " + puntoAlcanzado.toString());
                cpu.getPosicionesDisparables().remove(puntoAlcanzado);
            }
//        }
    }
}
