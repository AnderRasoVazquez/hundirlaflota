package codigo.cpu;

/**
 * Esta interfaz se usa para implementar el patr�n de dise�o ESTATE para la cpu
 */
public interface IEstadoCPU {

    void actuar();
}
