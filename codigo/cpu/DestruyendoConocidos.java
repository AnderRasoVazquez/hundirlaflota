package codigo.cpu;

import codigo.Casilla;

import java.util.ArrayList;

/**
 * Este estado intenta destruir las posiciones en las que sabemos que hay barcos
 */
public class DestruyendoConocidos implements IEstadoCPU {

    private CPU cpu;

    public DestruyendoConocidos(CPU cpu) {
        this.cpu = cpu;
    }

    @Override
    public void actuar() {
        System.out.println("Estado: destruyendo conocidos");
        Casilla puntoParaDisparar = this.getPuntoParaDisparar();
        if (puntoParaDisparar != null) {
            this.disparar(puntoParaDisparar);
        } else {
            cpu.setEstado(cpu.getEstadoDetectandoBarcos());
            cpu.actuar();
        }
    }

    /**
     * Funcion de disparo general
     * @param pPuntoParaDisparar
     */
    private void disparar(Casilla pPuntoParaDisparar){
        if (cpu.getArsenal().getArma(1).getMunicion() == 0) {
            if (cpu.getArsenal().getDinero() >= cpu.getArsenal().getArma(1).getPrecio()){
                cpu.getArsenal().getArma(1).abastecer(1);
                this.disparoMisil(pPuntoParaDisparar);
            } else {
                cpu.getArsenal().equiparArma(0);
                if (!cpu.getFlotaOponente().barcoEnCasilla(pPuntoParaDisparar).estaEscudado()) {
                    cpu.getPosicionesDisparables().remove(pPuntoParaDisparar);
                }
                cpu.atacar(pPuntoParaDisparar);
            }
        } else {
            System.out.println("hay municion para el misil");
            this.disparoMisil(pPuntoParaDisparar);
        }
    }

    private Casilla getPuntoParaDisparar(){
        Casilla puntoParaDisparar = null;
        for (Casilla punto: cpu.getPosicionesDisparables()) {
            if (cpu.getPosicionesConBarcoEnemigo().contains(punto)){
                puntoParaDisparar = punto;
                break;
            }
        }
        return puntoParaDisparar;
    }

    /**
     * Funcion de disparo especifica para misiles
     * @param pPuntoParaDisparar
     */
    private void disparoMisil(Casilla pPuntoParaDisparar) {
        cpu.getArsenal().equiparArma(1);
        if (!cpu.getFlotaOponente().barcoEnCasilla(pPuntoParaDisparar).estaEscudado()) {
            cpu.getPosicionesDisparables().remove(pPuntoParaDisparar);
            cpu.eliminarPuntosIncompatibles((ArrayList<Casilla>)
                            cpu.getFlotaOponente().barcoEnCasilla(pPuntoParaDisparar).getCasillas(),
                    cpu.getPosicionesDisparables(), true);
        }
        cpu.atacar(pPuntoParaDisparar);
    }
}
