package codigo.cpu;

import codigo.Casilla;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

/**
 * Este es un estado para la CPU en el que colocar� los radares de forma estrat�gica para conseguir desvelar el
 * contenido del mayor n�mero de casillas posible.
 */
public class DetectandoBarcos implements IEstadoCPU {

    private CPU cpu;

    /**
     * Lista de puntos interesantes donde colocar el radar.
     */
    private ArrayList<Casilla> puntosOptimos;

    /**
     * Constructora.
     * @param cpu
     */
    public DetectandoBarcos(CPU cpu){
        this.cpu = cpu;
        this.puntosOptimos = new ArrayList<Casilla>();
        this.addPuntosOptimos();
    }

    @Override
    public void actuar() {
        System.out.println("Estado: detectando Barcos");
        // si no tengo munici�n de radar cambio de estado y actuo
        if (cpu.getArsenal().getArma(5).getMunicion() > 0) {
            cpu.getArsenal().equiparArma(5);
            Collection<Casilla> puntosAlcanzados = cpu.atacar(puntoOptimoAlAzar());
            this.procesarPuntosAlcanzados(puntosAlcanzados);
            cpu.setEstado(cpu.getEstadoDestruyendoConocidos());
        } else {
            cpu.setEstado(cpu.getEstadoBuscandoBarco());
            cpu.actuar();
        }
    }

    /**
     * Si el punto tiene un barco lo a�ade a la lista de casillas con barco si no tiene lo quita de las listas
     * disparables.
     * @param pPuntosAlcanzados
     */
    private void procesarPuntosAlcanzados(Collection<Casilla> pPuntosAlcanzados){
        for (Casilla punto: pPuntosAlcanzados) {
            if (cpu.getFlotaOponente().casillaConBarco(punto)) {
                cpu.getPosicionesConBarcoEnemigo().add(punto);
            } else {
                cpu.getPosicionesDisparables().remove(punto);
            }
        }
    }

    /**
     * Devuelve un punto �ptimo al azar
     * @return
     */
    private Casilla puntoOptimoAlAzar(){
        Random random = new Random();
        Casilla puntoOptimo = this.puntosOptimos.get(random.nextInt(this.puntosOptimos.size()));
        this.puntosOptimos.remove(puntoOptimo);
        return puntoOptimo;
    }

    /**
     * A�ade a la lista de puntos �ptimos una serie de puntos que no interfieren entre s� y que dan una gran �rea
     * de puntos resueltos.
     */
    private void addPuntosOptimos() {
        this.puntosOptimos.add(new Casilla(2, 2));
        this.puntosOptimos.add(new Casilla(2, 7));
        this.puntosOptimos.add(new Casilla(7, 2));
        this.puntosOptimos.add(new Casilla(5, 4));
        this.puntosOptimos.add(new Casilla(7, 7));
    }
}
