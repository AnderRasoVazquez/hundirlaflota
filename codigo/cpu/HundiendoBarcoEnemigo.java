package codigo.cpu;

import java.util.ArrayList;

import codigo.Casilla;

/**
 * Este es un estado para la cpu en el que sabiendo una casilla perteneciente a un barco enemigo, intentar� hundirlo
 * por completo.
 */
public class HundiendoBarcoEnemigo implements IEstadoCPU {

    private CPU cpu;
    /**
     * Orientación:
     * True = vertical
     * False = horizontal
     * Es de tipo boolean porque resulta sencillo invertirlo mediante el operador '!'
     */
    private boolean orientacion;
    /**
     * Si la orientación es True (vertical):
     *  - True: arriba
     *  - False: abajo
     * Si la orientación es False (horizontal):
     *  - True: derecha
     *  - False: izquierda
     * Es de tipo boolean porque resulta sencillo invertirlo mediante el operador '!'
     */
    private boolean direccion;

    /**
     * Se usa para saber si estamos buscando la orientación del barco o ya la sabemos
     */
    private boolean buscandoOrientacion;

    /**
     * El primer disparo al cambiar de dirección tiene que ser a una casilla de distancia de la casilla pivote en la
     * dirección contraria, pero cuando no es el primer disparo hay que seguir desplazándose
     */
    private boolean primerDisparoAlCambiarDireccion;

    /**
     * Al acertar hay que moverse
     */
    private Casilla puntoActual;

    /**
     * Constructora
     * @param cpu
     */
    public HundiendoBarcoEnemigo(CPU cpu){
        this.cpu = cpu;
        this.restaurarEstadoInicial();
    }

    /**
     * Dispara hasta hundir el barco.
     */
    @Override
    public void actuar() {
        System.out.println("Estado: hundiendo barco enemigo");
        if (this.buscandoOrientacion) {
            this.buscarOrientacion();
        } else {
            // cuando ya sabemos la orientacion hay que empezar a hundir el barco
            this.hundirElBarco();
        }
    }

    /**
     * Restaura las variables a su forma inicial
     */
    private void restaurarEstadoInicial() {
        this.puntoActual = null;
        this.buscandoOrientacion = true;
        this.primerDisparoAlCambiarDireccion = true;
    }

    // funciones que devuelven la casilla adyacente {{{
    private Casilla puntoDerecha(Casilla punto){ return new Casilla(((int) punto.getX()), ((int) punto.getY()) + 1); }

    private Casilla puntoIzquierda(Casilla punto){ return new Casilla(((int) punto.getX()), ((int) punto.getY()) - 1); }

    private Casilla puntoArriba(Casilla punto){ return new Casilla(((int) punto.getX() - 1), ((int) punto.getY())); }

    private Casilla puntoAbajo(Casilla punto){ return new Casilla(((int) punto.getX() + 1), ((int) punto.getY())); }
    // funciones que devuelven la casilla adyacente }}}

    /**
     * Dispara a una casilla
     * @param pPunto
     * @param pOrientacion
     * @param pDireccion
     * @param pPrimerDisparoAlCambiarDireccion
     */
    private void atacarPuntoDisparable(Casilla pPunto, boolean pOrientacion, boolean pDireccion, boolean pPrimerDisparoAlCambiarDireccion) {
        System.out.println("Disparo a" + pPunto);
        // se puede disparar
        // pero hay un barco? si lo hay poner en posiciones con barco enemigo y avisar que se sabe la orientacion
        if (cpu.getFlotaOponente().casillaConBarco(pPunto)) {
            if (!cpu.getFlotaOponente().barcoEnCasilla(pPunto).estaEscudado()) {
                cpu.getPosicionesDisparables().remove(pPunto);
            }
            cpu.getPosicionesConBarcoEnemigo().add(pPunto);
            this.puntoActual = pPunto;
            this.buscandoOrientacion = false;
            this.orientacion = pOrientacion; // lo orientacion es vertical TODO ahora implementar fuera del if del buscando orientacion
            this.direccion = pDireccion; // vamos hacia arriba
            this.primerDisparoAlCambiarDireccion = pPrimerDisparoAlCambiarDireccion;
        } else {
            cpu.getPosicionesDisparables().remove(pPunto);
        }
        cpu.atacar(pPunto);
    }

    /**
     * Dispara en las casillas en las que podría estar el resto del barco.
     */
    private void buscarOrientacion(){
        Casilla punto = puntoArriba(cpu.getPosicionPivote());
        if(cpu.getPosicionesDisparables().contains(punto)){
            this.atacarPuntoDisparable(punto, true, true, true);
        } else {
            punto = puntoAbajo(cpu.getPosicionPivote());
            if(cpu.getPosicionesDisparables().contains(punto)) {
                this.atacarPuntoDisparable(punto, true, false, false);
            } else {
                punto = puntoDerecha(cpu.getPosicionPivote());
                if(cpu.getPosicionesDisparables().contains(punto)) {
                    this.atacarPuntoDisparable(punto, false, true, true);
                } else {
                    punto = puntoIzquierda(cpu.getPosicionPivote());
                    if(cpu.getPosicionesDisparables().contains(punto)) {
                        this.atacarPuntoDisparable(punto, false, false, false);
                    } else {
                        // hemos probado todas las direcciones y no hay casillas disponibles
                        // cambiamos al estado de buscar barco y actuamos directamente
                        System.out.println("No hay posiciones alrededor para disparar, el barco de una casilla está hundido");
                        this.limpiar();
                        cpu.actuar();
                    }
                }
            }
        }
    }

    /**
     * Elimina las casillas adyacentes donde por las normas del juego no puede haber un barco y devuelve las variables
     * del estado a us forma original
     */
    private void limpiar() {
        cpu.eliminarPuntosIncompatibles((ArrayList<Casilla>)
                        cpu.getFlotaOponente().barcoEnCasilla(cpu.getPosicionPivote()).getCasillas(),
                cpu.getPosicionesDisparables(), false);
        this.restaurarEstadoInicial();
        cpu.setEstado(cpu.getEstadoDestruyendoConocidos());
    }

    /**
     * Disparo para las direcciones abajo e izquierda
     * @param pPunto
     */
    private void disparoParaDireccionContraria(Casilla pPunto){
        if(cpu.getPosicionesDisparables().contains(pPunto)) {
            System.out.println("Disparo a" + pPunto);
            // se puede disparar
            // pero hay un barco? si lo hay poner en posiciones con barco enemigo y avisar que se sabe la orientacion
            if (cpu.getFlotaOponente().casillaConBarco(pPunto)) {
                if (!cpu.getFlotaOponente().barcoEnCasilla(pPunto).estaEscudado()) {
                    cpu.getPosicionesDisparables().remove(pPunto);
                }
                cpu.getPosicionesConBarcoEnemigo().add(pPunto);
                this.puntoActual = pPunto;
            } else { // si no hay barco significa que lo hemos destruido
                cpu.getPosicionesDisparables().remove(pPunto);
                System.out.println("No hay mas posiciones para disparar, barco hundido");
                this.limpiar();
            }
            cpu.atacar(pPunto);
        } else {
            // hemos probado todas las direcciones y no hay casillas disponibles
            System.out.println("No hay mas posiciones para disparar, barco hundido");
            this.limpiar();
            cpu.actuar();
        }
    }

    /**
     * Disparo para las direcciones arriba y derecha
     * @param pPunto
     */
    private void disparoPorDefecto(Casilla pPunto) {
        if(cpu.getPosicionesDisparables().contains(pPunto)) {
            System.out.println("Disparo a" + pPunto);
            // se puede disparar
            // pero hay un barco? si lo hay poner en posiciones con barco enemigo y avisar que se sabe la orientacion
            if (cpu.getFlotaOponente().casillaConBarco(pPunto)) {
                if (!cpu.getFlotaOponente().barcoEnCasilla(pPunto).estaEscudado()) {
                    cpu.getPosicionesDisparables().remove(pPunto);
                }
                cpu.getPosicionesConBarcoEnemigo().add(pPunto);
                this.puntoActual = pPunto;
            } else { // si no hay barco
                cpu.getPosicionesDisparables().remove(pPunto);
                this.direccion = !this.direccion; // ahora que no esta esto cambiamos de direccion
            }
            cpu.atacar(pPunto);
        } else {
            this.direccion = !this.direccion; // ahora que no esta esto cambiamos de direccion
            cpu.actuar();
        }
    }

    /**
     * Una vez que se sabe la orientacion del barco lo irá destruyendo
     */
    private void hundirElBarco(){
        Casilla punto;
        if (this.orientacion) { // si es vertical
            if (this.direccion) { // si es para arriba
                punto = puntoArriba(this.puntoActual);
                this.disparoPorDefecto(punto);
            } else { // si es para abajo
                if (this.primerDisparoAlCambiarDireccion) {
                    punto = puntoAbajo(cpu.getPosicionPivote());
                    this.primerDisparoAlCambiarDireccion = false;
                } else {
                    punto = puntoAbajo(this.puntoActual);
                }
                this.disparoParaDireccionContraria(punto);
            }
        } else { // si es horizontal
            if (this.direccion) { // si es para derecha
                punto = puntoDerecha(this.puntoActual);
                this.disparoPorDefecto(punto);
            } else { // si es para izquierda
                if (this.primerDisparoAlCambiarDireccion) {
                    punto = puntoIzquierda(cpu.getPosicionPivote());
                    this.primerDisparoAlCambiarDireccion = false;
                } else {
                    punto = puntoIzquierda(this.puntoActual);
                }
                this.disparoParaDireccionContraria(punto);
            }
        }
    }
}
