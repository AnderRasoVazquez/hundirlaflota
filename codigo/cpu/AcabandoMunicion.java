package codigo.cpu;

import codigo.Casilla;
import codigo.barcos.Barco;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Estado en el que se intenta gastar la munici�n de las armas Boom, NS, EO
 */
public class AcabandoMunicion implements IEstadoCPU {

    private CPU cpu;

    public AcabandoMunicion(CPU cpu) {
        this.cpu = cpu;
    }

    @Override
    public void actuar() {
        System.out.println("Estado: acabando la munici�n");
        // boom 4
        // eo 3
        // ns 2
        if (this.hayMunicion(4)){
            this.disparar(4);
        } else if (this.hayMunicion(3)){
            this.disparar(3);
        } else if (this.hayMunicion(2)){
            this.disparar(2);
        } else {
            cpu.setEstado(cpu.getEstadoDestruyendoConocidos());
            cpu.actuar();
        }

    }

    public void disparar(int pId){
        cpu.getArsenal().equiparArma(pId);
        // elegir casilla libre random
        Collection<Casilla> casillasAlcanzadas = cpu.atacar(cpu.getPuntoDisparableAlAzar());

        for (Casilla casilla: casillasAlcanzadas) {
            /*
            si la casilla NO tiene barco quitarla de la lista de posiciones disparables
            si la casilla SI tiene barco
                coger las casillas del barco
                a�adir las casillas a la lista de posiciones donde sabemos que hay barco
                si esta destruido
                    quitar de la lista de posiciones disparables tanto la suya como la que le rodea
                si no
                    solo quitar las adyacentes
             */
            if (!cpu.getFlotaOponente().casillaConBarco(casilla)){
                cpu.getPosicionesDisparables().remove(casilla);
            } else { // si hay un barco
                Barco barco = cpu.getFlotaOponente().barcoEnCasilla(casilla);
                Collection<Casilla> casillasDelBarco = barco.getCasillas();
                for (Casilla casillaDeBarco: casillasDelBarco) {
                    cpu.addCasillaSinRepetir(casillaDeBarco, cpu.getPosicionesConBarcoEnemigo());
                    if (barco.estaDestruido()) {
                        cpu.eliminarPuntosIncompatibles((ArrayList<Casilla>) barco.getCasillas(), cpu.getPosicionesDisparables(), true);
                    } else {
                        cpu.eliminarPuntosIncompatibles((ArrayList<Casilla>) barco.getCasillas(), cpu.getPosicionesDisparables(), false);
                    }
                }
            }
        }
    }

    /**
     * Devuelve true si hay municion
     * @param pId del arma
     * @return
     */
    public boolean hayMunicion(int pId){
        return cpu.getArsenal().getArma(pId).getMunicion() > 0;

    }
}
