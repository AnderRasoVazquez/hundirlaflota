package codigo.cpu;

import codigo.Casilla;
import codigo.Juego;
import codigo.Jugador;
import codigo.barcos.Barco;

import java.util.ArrayList;
import java.util.Random;

/**
 * Esta clase representa al ordenador contra el que se juega al hundir la flota
 */
public class CPU extends Jugador {

    /**
     * Las posiciones libres representan las coordenadas donde se podría colocar un barco. A medida que se van
     * asignando posiciones a los barcos el número de posiciones libres de esta lista va disminuyendo, ya que
     * la ocupa el propio barco o las coordenadas adyacentes a él.
     */
    private ArrayList<Casilla> posicionesDondeColocarBarco = new ArrayList<Casilla>();

    /**
     * Las posiciones disparables son las posiciones en las que se puede disparar.
     */
    private ArrayList<Casilla> posicionesDisparables = new ArrayList<Casilla>();

    /**
     * Esta lista guardará todas las posiciones donde haya un barco enemigo. Esto es útil, por ejemplo, para cuando
     * hemos dejodo tocado a un barco enemigo y este se cura:
     * Cuando atacamos a un barco quitamos de la lista de posiciones disparables los sitios donde hemos disparado, pero
     * si un enemigo cura un barco, significa que ahora esas posiciones vuelven a estar disponibles. Como ya sabemos que
     * en esas había un barco, miraremos si la lista de posiciones disparables contiene una en la que haya un barco,
     * para ello, comprobaremos las casillas disparables con esta lista.
     */
    private ArrayList<Casilla> posicionesConBarcoEnemigo = new ArrayList<Casilla>();

    /**
     * Posición donde al disparar se ha encontrado un barco, esta variable se usa para pivotar desde ella hacia
     * las posiciones donde es posible que siga el barco, para hundirlo de forma inteligente.
     */
    private Casilla posicionPivote;

    // estados posibles
    private IEstadoCPU estadoBuscandoBarco = new BuscandoBarcoEnemigo(this);
    private IEstadoCPU estadoHundiendoBarco = new HundiendoBarcoEnemigo(this);
    private IEstadoCPU estadoDetectandoBarcos = new DetectandoBarcos(this);
    private IEstadoCPU estadoDestruyendoConocidos = new DestruyendoConocidos(this);
    private IEstadoCPU estadoAcabandoMunicion = new AcabandoMunicion(this);
    private IEstadoCPU estado;  // estado actual

    /**
     * Constructora de cpu
     */
    public CPU(String pNombre) {
        super(pNombre);
        this.posicionesDondeColocarBarco = new ArrayList<>();
        this.iniciarPosiciones(this.posicionesDondeColocarBarco);
        this.iniciarPosiciones(this.posicionesDisparables);
        this.estado = this.estadoAcabandoMunicion;
    }

    /**
     * Constructora.
     */
    public CPU() {
        this("cpu");
    }

    /**
     * La cpu tendrá un comportamiento diferente dependiendo del estado en el que se encuentre.
     *
     * La CPU se comporta de la siguiente manera:
     *  + En cada turno hay un 20% de probabilidades de que intente poner un escudo o curar un barco.
     *  + Siempre va a intentar eliminar primero a base de misiles los barcos cuyo paradero conoce.
     *  + Empieza en el estado AcabandoMunicion, en el que irá gastando toda la artillería pesada.
     *  + Cuando gasta el armamento pone radares de una forma inteligente (para descubrir el máximo número de casillas)
     *  y por cada barco que vea lo derriba de un misil, si hace falta compra más.
     *  + Cuando se le acaban los radares entra en el estado BscandoBarcoEnemigo, en el que dispara de forma aleatoria
     *  hasta que encuentra un barco.
     *  + Cuando lo encuentra, entra en el estado HundiendoBarcoEnemigo, en el que lo irá rodeando a disparos para
     *  descubrir su orientación y derribarlo por completo.
     */
    public void actuar() {
        Random generator = new Random();
        int opt = generator.nextInt(5) + 1; // random del 1 al 5
        /*
        opt = 1: poner escudo
        opt = 2: curar un barco
        opt > 2: actuar
         */
        this.ejecutarAccionRandom(opt);
        estado.actuar();
//        System.out.println(this.returnTablero());  // util para ver como dispara en la terminal
    }

    /**
     * Cura un barco o le pone un escudo
     * @param pOpt
     * @return
     */
    private boolean ejecutarAccionRandom(int pOpt) {
        boolean sePudoEjecutarLaAccion = false;
        if (pOpt == 1) {
            // poner escudo
            for(Barco barco: this.getFlota().getBarcos()) {
                if (!barco.estaDestruido()) {
                    ArrayList<Casilla> casillasBarco = (ArrayList<Casilla>) barco.getCasillas();
                    sePudoEjecutarLaAccion = this.activarEscudo(casillasBarco.get(0));
                    break;
                }
            }
        } else if (pOpt == 2) {
            // curar barco
            for(Barco barco: this.getFlota().getBarcos()) {
                if (barco.estaDanado()){
                    ArrayList<Casilla> casillasBarco = (ArrayList<Casilla>) barco.getCasillas();
                    sePudoEjecutarLaAccion = this.repararBarco(casillasBarco.get(0));
                    break;
                }
            }
        }
        return sePudoEjecutarLaAccion;
    }

    /**
     * Devuelve las casillas llenas
     * @return
     */
    public String returnTablero() {
        String tablero = "    | 0 || 1 || 2 || 3 || 4 || 5 || 6 || 7 || 8 || 9 |\n";
        System.out.println();
        for (int fila = 0; fila < Juego.ALTURA_TABLERO; fila++) {
            tablero+="| " + fila + " ";
            for (int col = 0; col < Juego.ANCHURA_TABLERO; col++) {
                if (this.getPosicionesDisparables().contains(new Casilla(fila, col))){
                    tablero += "|   |";
                } else {
                    tablero += "| * |";
                }
            }
            tablero += "\n";
        }
        return tablero;

    }

    /**
     * Función a la que llamar cuando se ha reparado un barco y queremos tener de nuevo esas posiciones
     * disponibles para atacarlas.
     *
     * #################################################################################################################
     * TODO esto creo que hay que implementarlo con un observer. Cuando el Humano se cure, la CPU llame a este metodo de
     * la siguiente manera:
     * por cada casilla del barco que se ha curado:
     *      cpu.addCasillaSinRepetir(casilla, cpu.getPosicionesDisparables())
     *  Con esto debería ser suficiente
     * #################################################################################################################
     */
    public void addCasillaSinRepetir(Casilla pCasilla, ArrayList<Casilla> pLista) {
        ArrayList<Casilla> casillas = (ArrayList<Casilla>) this.getFlotaOponente().barcoEnCasilla(pCasilla).getCasillas();
        for (Casilla casilla: casillas) {
            if (!pLista.contains(casilla)){
                pLista.add(casilla);
            }
        }
    }

    /**
     * Rellena la lista de posiciones libres dependiendo de las dimensiones del tablero.
     */
    private void iniciarPosiciones(ArrayList<Casilla> posiciones) {
        for (int i = 0; i < Juego.ALTURA_TABLERO; i++) {
            for (int j = 0; j < Juego.ANCHURA_TABLERO; j++) {
                posiciones.add(new Casilla(i, j));
            }
        }
    }

    /**
     * Coloca todos los barcos de la Flota en sus respectivas posiciones,
     *
     * @return true si se han podido colocar los barcos, false en caso contrario
     */
    public boolean colocarBarcos() {
        int[] longitudesBarcos = this.getFlota().getLongitudesBarcos();
        int maximo_intentos = 100; /*
                                    * Mecanismo de seguridad para evitar un bucle infinito en caso de que no
                                    * hubiera una posición libre
                                    */
        int i = 0;
        while (i < longitudesBarcos.length) {
            int barcoActual = longitudesBarcos[i];
            boolean se_ha_colocado = this.colocarBarco(barcoActual, maximo_intentos);
            if (!se_ha_colocado) {
                return false; /*
                               * Se ha dado un caso improbable en el que la colocación aleatoria de barcos no
                               * ha dejado espacio en el "tablero" para otro barco más
                               */
            }

            i++;
        }

        return true;
    }

    /**
     * Coloca un barco de forma aleatoria donde tenga espacio.
     * <ul>
     * <li>Por cada barco busca una posicion libre al azar de la lista de posiciones libres.</li>
     * <li>Al azar decide si va a ser colocado de forma horizontal o vertical.</li>
     * <li>Si hay espacio lo coloca, si no lo hay busca una nueva posición y orientación para probar de nuevo.
     * </li>
     * </ul>
     *
     * @param pLongitudBarcoActual Longitud del barco que se quiere colocar
     * @param pMaximo_intentos     Máximo de oportunidades que tiene para intentar colocarse
     * @return true si se ha colocado bien, false si no hay espacio para que se calaque
     */
    private boolean colocarBarco(int pLongitudBarcoActual, int pMaximo_intentos) {
        boolean colocado = false;
        int contadorDeIntentos = 0;
        while (!colocado && contadorDeIntentos < pMaximo_intentos) {
            contadorDeIntentos++;
            Casilla puntoAlAzar = this.getPuntoLibreAlAzar();
            boolean vertical = this.getOrientacion();
            if (hayEspacio(pLongitudBarcoActual, puntoAlAzar, vertical)) {
                asignarPosicionesAlBarco(pLongitudBarcoActual, puntoAlAzar, vertical);
                colocado = true;
            }
        }
        return colocado;
    }

    /**
     * Devuelve una orientación al azar para el barco, horizontal o vertical.
     *
     * @return un booleano al azar
     */
    private boolean getOrientacion() {
        Random random = new Random();
        return random.nextInt(2) == 0;
    }

    /**
     * Devuelve un punto al azar de la lista de posiciones libres.
     *
     * @return un punto al azar.
     */
    private Casilla getPuntoLibreAlAzar() {
        Random random = new Random();
        return this.posicionesDondeColocarBarco.get(random.nextInt(this.posicionesDondeColocarBarco.size()));
    }

    /**
     * Devuelve un punto al azar de la lista de posiciones disparables.
     *
     * @return un punto al azar.
     */
    public Casilla getPuntoDisparableAlAzar() {
        Random random = new Random();
        return this.posicionesDisparables.get(random.nextInt(this.posicionesDisparables.size()));
    }

    /**
     * Comprueba si hay espacio en un punto para un barco en una determinada orientación.
     *
     * @param pLongitudBarcoActual Longitud del barco del que se quiere saber si tendría espacio.
     * @param pPunto               Punto desde donde se comprabará si hay espacio.
     * @param pVertical            orientación en la que se recorrerán los puntos a comprobar.
     * @return true si hay espacio, false si no lo hay.
     */
    private boolean hayEspacio(int pLongitudBarcoActual, Casilla pPunto, boolean pVertical) {
        int fila = (int) pPunto.getX();
        int columna = (int) pPunto.getY();
        boolean hayEspacio = true;
        for (int i = 0; i < pLongitudBarcoActual; i++) {
            if (pVertical) {
                if (!posicionesDondeColocarBarco.contains(new Casilla(fila, columna + i))) {
                    hayEspacio = false;
                    break;
                }
            } else {
                if (!posicionesDondeColocarBarco.contains(new Casilla(fila + i, columna))) {
                    hayEspacio = false;
                    break;
                }
            }
        }
        return hayEspacio;
    }

    /**
     * Dado un barco, un punto y la orientación en la que queremos colocar un barco, le asigna las posiciones
     * correctas.
     *
     * @param pLongitudBarcoActual Longitud del barco al que queremos asignarle posiciones.
     * @param pPunto               Punto desde el que se asignarán las posiciones
     * @param pVertical            Orientación en la que se recorrerán los puntos a colocar.
     */
    private void asignarPosicionesAlBarco(int pLongitudBarcoActual, Casilla pPunto, boolean pVertical) {
        int fila = (int) pPunto.getX();
        int columna = (int) pPunto.getY();
        ArrayList<Casilla> puntosQueOcupaElBarco = new ArrayList<>();

        for (int i = 0; i < pLongitudBarcoActual; i++) {
            if (pVertical) {
                puntosQueOcupaElBarco.add(new Casilla(fila, columna + i));
            } else {
                puntosQueOcupaElBarco.add(new Casilla(fila + i, columna));
            }
        }

        this.getFlota().colocarBarco(puntosQueOcupaElBarco);
        this.eliminarPuntosIncompatibles(puntosQueOcupaElBarco, this.posicionesDondeColocarBarco, true);
    }

    /**
     * Dada una lista de puntos asignados a un barco, elimina dichos puntos y los adyacentes a estos de la
     * lista de posiciones libres.
     *
     * @param pPuntosParaEliminar lista de puntos asignados a un barco
     */
    public void eliminarPuntosIncompatibles(ArrayList<Casilla> pPuntosParaEliminar, ArrayList<Casilla> pListaDondeEliminar, boolean pEliminarBarco) {
        for (Casilla punto : pPuntosParaEliminar) {
            ArrayList<Casilla> puntosAdyacentes = this.getPuntosAdyacentes(punto);
            for (Casilla p : puntosAdyacentes) {
                if(!pEliminarBarco) {
                    if (!pPuntosParaEliminar.contains(p)){
                        pListaDondeEliminar.remove(p);
                    }
                } else {
                    pListaDondeEliminar.remove(p);
                }
            }
        }
    }

    /**
     * Dado un punto, devuelve una lista con el propio punto y los adyacentes a una casilla de este.
     *
     * @param pPunto punto del que se quieren saber los adyacentes.
     * @return una lista con el propio punto y los adyacentese a este a una casilla de distancia
     */
    private ArrayList<Casilla> getPuntosAdyacentes(Casilla pPunto) {
        int x = (int) pPunto.getX();
        int y = (int) pPunto.getY();
        ArrayList<Casilla> puntosAdyacentes = new ArrayList<>();
        for (int i = x - 1; i <= x + 1; i++) {
            for (int j = y - 1; j <= y + 1; j++) {
                puntosAdyacentes.add(new Casilla(i, j));
            }
        }
        return puntosAdyacentes;
    }

    /**
     * Dado un punto, devuelve una lista de casillas donde podría estar el barco.
     *
     * @param pPunto
     * @return
     */
    public ArrayList<Casilla> getPuntosAdyacentesDisparables(Casilla pPunto) {
        int fila = (int) pPunto.getX();
        int columna = (int) pPunto.getY();
        ArrayList<Casilla> puntosAlrededor = new ArrayList<Casilla>();
        Casilla p1 = new Casilla(fila, columna - 1); // izquierda
        if (this.posicionesDisparables.contains(p1)) {
            puntosAlrededor.add(p1);
        }
        Casilla p2 = new Casilla(fila, columna + 1); // derecha
        if (this.posicionesDisparables.contains(p2)) {
            puntosAlrededor.add(p2);
        }
        Casilla p3 = new Casilla(fila - 1, columna); // arriba
        if (this.posicionesDisparables.contains(p3)) {
            puntosAlrededor.add(p3);
        }
        Casilla p4 = new Casilla(fila + 1, columna); // abajo
        if (this.posicionesDisparables.contains(p4)) {
            puntosAlrededor.add(p4);
        }
        return puntosAlrededor;
    }

    public IEstadoCPU getEstadoHundiendoBarco() {
        return estadoHundiendoBarco;
    }

    public IEstadoCPU getEstadoBuscandoBarco() {
        return estadoBuscandoBarco;
    }

    public IEstadoCPU getEstadoDetectandoBarcos() { return estadoDetectandoBarcos; }

    public IEstadoCPU getEstadoDestruyendoConocidos() { return estadoDestruyendoConocidos; }

    public IEstadoCPU getEstadoAcabandoMunicion() { return estadoAcabandoMunicion; }


    public void setEstado(IEstadoCPU estado) {
        this.estado = estado;
    }

    public Casilla getPosicionPivote() {
        return posicionPivote;
    }

    public void setPosicionPivote(Casilla posicionPivote) {
        this.posicionPivote = posicionPivote;
    }

    public ArrayList<Casilla> getPosicionesDisparables() {
        return posicionesDisparables;
    }

    @Override
    public void comenzarTurno() {
        super.comenzarTurno();
        actuar();
        //y lo que quieras
        terminarTurno();
    }

    /**
     * Metodo que sirve para hacer que la cpu dispare
     */
    public void comenzarTurnoTest() {
        super.setAccionEspecial(false);
        super.setDisparado(false);
        super.setTurnoActivo(true);
        actuar();
    }

    // ############### Funciones para usar con los test de junit ################### {{{
    public boolean getOrientacionTest() {
        return this.getOrientacion();
    }

    public ArrayList<Casilla> getPosicionesLibresTest() {
        return posicionesDondeColocarBarco;
    }

    public Casilla getPuntoLibreAlAzarTest() {
        return this.getPuntoLibreAlAzar();
    }

    public void eliminarPuntosIncompatiblesTest(ArrayList<Casilla> pPuntosParaEliminar) {
        this.eliminarPuntosIncompatibles(pPuntosParaEliminar, this.posicionesDondeColocarBarco, true);
    }

    public ArrayList<Casilla> getPuntosAdyacentesTest(Casilla pPunto) {
        return this.getPuntosAdyacentes(pPunto);
    }

    public ArrayList<Casilla> getPosicionesConBarcoEnemigo() {
        return posicionesConBarcoEnemigo;
    }

    // ############### Funciones para usar con los test de junit ################### }}}
}