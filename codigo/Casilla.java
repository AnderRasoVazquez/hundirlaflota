package codigo;

import java.awt.*;

@SuppressWarnings("serial")
public class Casilla extends Point {

    public Casilla(int x, int y) {
        super(x, y);
    }

    @Override
    public String toString() {
        return "(" + (int) getX() + ", " + (int) getY() + ")";
    }

    public Casilla getLocation() {
        return new Casilla(this.x, this.y);
    }
}
