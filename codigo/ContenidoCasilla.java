package codigo;

public enum ContenidoCasilla {

    AGUA, BARCO, ESCUDO, DANADO;

    private ContenidoCasilla() {

    }

    public String toString() {
        String ret;
        switch (this) {
            case AGUA:
                ret = "Agua";
                break;
            case BARCO:
                ret = "Barco";
                break;
            case ESCUDO:
                ret = "Escudo";
                break;
            case DANADO:
                ret = "Danado";
                break;
            default:
                ret = "error";
                break;
        }
        return ret;
    }
}
