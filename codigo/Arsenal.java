package codigo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Observable;

import codigo.armas.Armamento;
import codigo.armas.Bombas;
import codigo.armas.Misiles;
import codigo.armas.Misiles_BOOM;
import codigo.armas.Misiles_EO;
import codigo.armas.Misiles_NS;
import codigo.armas.Radar;
import codigo.barcos.Barco;
import codigo.excepciones.ArmaNoEquipadaException;
import codigo.excepciones.NotEnoughCashException;

/**
 * Esta clase representa el arsenal de un jugador. En ella se almacena la colección de armamento, y el dinero
 * a disposición del Jugador así como el arma equipara actualmente.
 */
public class Arsenal extends Observable {

    /**
     * Cantidad de dinero preestablecida con la que un jugador comienza la partida.
     */
    public static final int DINERO_INICIAL = 1000;

    /**
     * Coste de reparar un segmento de nu barco.
     */
    public static final int PRECIO_REPARACION = 60;

    /**
     * Coste de activar el escudo sobre un segmento.
     */
    public static final int PRECIO_ESCUDO = 40;
    /**
     * Colección de armas disponibles. Guarda una instancia de cada subclase de armamento.
     */
    private Collection<Armamento> coleccionArmas;
    /**
     * El arma equipada actualmente.
     */
    private Armamento armaEquipada;
    /**
     * Cantidad de dinero restante.
     */
    private int dinero;

    public Arsenal() {
        dinero = DINERO_INICIAL;
        coleccionArmas = new ArrayList<Armamento>(6);
        coleccionArmas.add(armaEquipada = new Bombas());
        coleccionArmas.add(new Misiles());
        coleccionArmas.add(new Misiles_NS());
        coleccionArmas.add(new Misiles_EO());
        coleccionArmas.add(new Misiles_BOOM());
        coleccionArmas.add(new Radar());
    }

    public int getDinero() {
        return dinero;
    }

    /**
     * Equipa el arma cuyo identificador coincida con el especificado.
     *
     * @param pIdArma
     *
     * @return true si el proceso se lleva a cabo sin errores;
     */
    public boolean equiparArma(int pIdArma) {
        Armamento arma = getArma(pIdArma);
        if (arma!= null) {
            armaEquipada = arma;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Dispara con el arma equipada a la casilla pCasilla de pFlota.
     *
     * @param pFlota
     * @param pCasilla
     * @return la lista de posiciones que han sido alcanzadas y, por tanto, reveladas.
     */
    public Collection<Casilla> disparar(Flota pFlota, Casilla pCasilla) throws ArmaNoEquipadaException {
        try {
            Collection<Casilla> casillas = armaEquipada.disparar(pFlota, pCasilla);
            if (casillas != null) {
                setChanged();
                notifyObservers(new NotifyArg("disparar", casillas));
            }
            return casillas;
        } catch (NullPointerException e) {
            throw new ArmaNoEquipadaException();
        }
    }

    /**
     * Repara el barco situado en pCasilla de pFlota.
     *
     * @param pFlota
     * @param pCasilla
     * @return true si la reparación se ha llevado a cabo con éxito, false en caso contrario.
     */
    public boolean repararBarco(Flota pFlota, Casilla pCasilla) throws NotEnoughCashException {
        Barco barco = pFlota.barcoEnCasilla(pCasilla);
        boolean exito = false;
        if (barco != null) {
            int coste = barco.segmetosDanados() * PRECIO_REPARACION;
            if (dinero >= coste) {
                if (exito = barco.reparar())
                    dinero -= coste;
                setChanged();
                notifyObservers(new NotifyArg("repararBarco", pCasilla));
            } else {
                throw new NotEnoughCashException();
            }
        }
        return exito;
    }

    /**
     * Activa el escudo del barco situado en pCasilla de pFLota.
     *
     * @param pFlota
     * @param pCasilla
     * @return true si la reparación se ha llevado a cabo con éxito, false en caso contrario.
     */
    public boolean activarEscudo(Flota pFlota, Casilla pCasilla) throws NotEnoughCashException {
        Barco barco = pFlota.barcoEnCasilla(pCasilla);
        boolean exito = false;
        if (barco != null) {
            int coste = barco.getNumSegmentos() * PRECIO_ESCUDO;
            if (dinero >= coste) {
                if (exito = barco.activarEscudo())
                    dinero -= coste;
                setChanged();
                notifyObservers(new NotifyArg("activarEscudo", pCasilla));
            } else {
                throw new NotEnoughCashException();
            }
        }
        return exito;
    }

    /**
     * Incrementa la munición del arma con id pId en pCantidad.
     *
     * @param pId
     * @param pCantidad
     * @return true si la operación se ha levado a cabo sin errores, false en caso contrario.
     */
    public boolean reabastecer(int pId, int pCantidad) throws NotEnoughCashException {
        Armamento arma = getArma(pId);
        boolean exito = false;
        if (arma != null && pCantidad > 0) {
            int coste = pCantidad * arma.getPrecio();
            if (dinero >= coste) {
                dinero -= coste;
                arma.abastecer(pCantidad);
                exito = true;
                setChanged();
                notifyObservers(new NotifyArg("reabastecer", dinero));
            } else {
                throw new NotEnoughCashException();
            }
        }
        return exito;
    }

    /**
     * @return true si el arma equipada tiene suficiente munición para disparar.
     */
    public boolean puedeDisparar() {
        return armaEquipada.puedeDisparar();
    }

    /**
     * @return Iterador para recorrer la colección de armamento.
     */
    private Iterator<Armamento> getIterador() {
        return coleccionArmas.iterator();
    }

    /**
     * @param pId
     * @return el arma con identificador pId.
     */
    public Armamento getArma(int pId) {
        Iterator<Armamento> it = getIterador();
        Armamento arma;
        while (it.hasNext()) {
            arma = it.next();
            if (arma.getId() == pId)
                return arma;
        }
        return null;
    }
    
    public Armamento getArmaEquipada() {
        return armaEquipada;
    }
}
