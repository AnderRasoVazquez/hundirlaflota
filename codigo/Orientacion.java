package codigo;

public enum Orientacion {

    NORTE, SUR, ESTE, OESTE;

    private Orientacion() {

    }

    public String toString() {
        String ret;
        switch (this) {
        case NORTE:
            ret = "Norte";
            break;
        case SUR:
            ret = "Sur";
            break;
        case ESTE:
            ret = "Este";
            break;
        case OESTE:
            ret = "Oeste";
            break;
        default:
            ret = "error";
            break;
        }
        return ret;
    }
}
