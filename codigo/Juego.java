package codigo;

import codigo.cpu.CPU;

import java.util.Observable;

/**
 * Esta clase singelton representa el conjuto entero del juego y almacena los dos jugadores.
 */
public class Juego extends Observable {

    public static final int ALTURA_TABLERO = 10;
    public static final int ANCHURA_TABLERO = 10;

    private static Juego elJuego;

    /**
     * Jugador 1, el Humano.
     */
    private Jugador J1;
    /**
     * Jugador 2, la cpu.
     */
    private Jugador J2;
    /**
     * Este contador se incrementa cada vez que un jugador termina su turno,
     * no cada vez que los dos jugadores jugan un turno.
     */
    private int turno;

    private boolean inicializado;

    private boolean finJuego;

    public static Juego getElJuego() {
        if (elJuego == null)
            elJuego = new Juego();
        return elJuego;
    }

    public static Juego getElJuego(String pNombreJugador, String pNombreCPU) {
        if (elJuego == null)
            elJuego = new Juego(pNombreJugador, pNombreCPU);
        return elJuego;
    }

    private Juego() {
       this("Metatron", "CPU");
    }

    private Juego(String pNombreJugador, String pNombreCPU) {
        J1 = new Humano(pNombreJugador);
        J2 = new CPU(pNombreCPU);
        inicializado = false;
        finJuego = false;
        turno = -1;
    }

    // Setters para Junit {{{
    public void setJ1(Jugador j1) {
        J1 = j1;
    }

    public void setJ2(Jugador j2) {
        J2 = j2;
    }
    // Setters para Junit }}}

    public Jugador getJ1() {
        return J1;
    }

    public Jugador getJ2() {
        return J2;
    }

    public int getTurno() {
        return turno / 2 + 1;
    }

    public void cambiarTurno() {
        if (!finJuego) {
            boolean j2Pierde;
            if (!(j2Pierde = J2.haPerdido()) && !(J1.haPerdido())) {
                turno++;
                setChanged();
                if (turno % 2 == 0) { // par -> se ha dado una vuelta completa
                    notifyObservers(new NotifyArg("cambiarTurno", J1.getNombre()));
                    J1.comenzarTurno();
                } else { // impar -> no se ha dado una vuelta completa
                    notifyObservers(new NotifyArg("cambiarTurno", J2.getNombre()));
                    J2.comenzarTurno();
                }
            } else {
                if (j2Pierde) {
                    finJuego(J1, J2);
                } else {
                    finJuego(J2, J1);
                }
            }
        }
    }

    /**
     * Inicializa el juego entero: crea los Jugadores y los inicializa.
     */
    public void inicializar() {
        if (!inicializado) {
            J1.inicializar(J2.getFlota());
            J2.inicializar(J1.getFlota());
            // siempre comienza el humano
            cambiarTurno();
        }
    }

    private void finJuego(Jugador pGanador, Jugador pPerdedor) {
        finJuego = true;
        J1.terminarTurno();
        J2.terminarTurno();
        String mensaje = "\n" + pGanador.getNombre() + " gana!\n" + pPerdedor.getNombre() + " pierde...";
        setChanged();
        notifyObservers(new NotifyArg("finJuego", mensaje));
    }

    public static void dibujarPosicionesTablero() {
        System.out.println(
                "_____________________________________________________________" +
                        "|     |     |     |     |     |     |     |     |     |     |" +
                        "| 0,0 | 0,1 | 0,2 | 0,3 | 0,4 | 0,5 | 0,6 | 0,7 | 0,8 | 0,9 |" +
                        "|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|" +
                        "|     |     |     |     |     |     |     |     |     |     |" +
                        "| 1,0 | 1,1 | 1,2 | 1,3 | 1,4 | 1,5 | 1,6 | 1,7 | 1,8 | 1,9 |" +
                        "|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|" +
                        "|     |     |     |     |     |     |     |     |     |     |" +
                        "| 2,0 | 2,1 | 2,2 | 2,3 | 2,4 | 2,5 | 2,6 | 2,7 | 2,8 | 2,9 |" +
                        "|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|" +
                        "|     |     |     |     |     |     |     |     |     |     |" +
                        "| 3,0 | 3,1 | 3,2 | 3,3 | 3,4 | 3,5 | 3,6 | 3,7 | 3,8 | 3,9 |" +
                        "|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|" +
                        "|     |     |     |     |     |     |     |     |     |     |" +
                        "| 4,0 | 4,1 | 4,2 | 4,3 | 4,4 | 4,5 | 4,6 | 4,7 | 4,8 | 4,9 |" +
                        "|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|" +
                        "|     |     |     |     |     |     |     |     |     |     |" +
                        "| 5,0 | 5,1 | 5,2 | 5,3 | 5,4 | 5,5 | 5,6 | 5,7 | 5,8 | 5,9 |" +
                        "|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|" +
                        "|     |     |     |     |     |     |     |     |     |     |" +
                        "| 6,0 | 6,1 | 6,2 | 6,3 | 6,4 | 6,5 | 6,6 | 6,7 | 6,8 | 6,9 |" +
                        "|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|" +
                        "|     |     |     |     |     |     |     |     |     |     |" +
                        "| 7,0 | 7,1 | 7,2 | 7,3 | 7,4 | 7,5 | 7,6 | 7,7 | 7,8 | 7,9 |" +
                        "|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|" +
                        "|     |     |     |     |     |     |     |     |     |     |" +
                        "| 8,0 | 8,1 | 8,2 | 8,3 | 8,4 | 8,5 | 8,6 | 8,7 | 8,8 | 8,9 |" +
                        "|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|" +
                        "|     |     |     |     |     |     |     |     |     |     |" +
                        "| 9,0 | 9,1 | 9,2 | 9,3 | 9,4 | 9,5 | 9,6 | 9,7 | 9,8 | 9,9 |" +
                        "|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|"
        );
    }

}
