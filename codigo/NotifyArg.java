package codigo;

public class NotifyArg {

    private String motivo;
    private Object arg;

    public NotifyArg(String pMotivo, Object pArg) {
        motivo = pMotivo;
        arg = pArg;
    }

    public String getMotivo() {
        return motivo;
    }

    public Object getArg() {
        return arg;
    }
}
