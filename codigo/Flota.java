package codigo;

import codigo.barcos.Barco;
import codigo.barcos.SimpleBarcoFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Observable;
import java.util.function.Consumer;

/**
 * Esta clase representa la flota de un jugador. Almacena y gestiona la colección de barcos.
 */
public class Flota extends Observable {

    /**
     * Colección de barcos que conforman la flota.
     */
    private Collection<Barco> barcos;
    /**
     * Número de portaaviones con los que comienza la flota.
     */
    private final static int N_PORTAAVIONES = 1;
    /**
     * Número de submarinos con los que comienza la flota.
     */
    private final static int N_SUBMARINOS = 2;
    /**
     * Número de destructores con los que comienza la flota.
     */
    private final static int N_DESTRUCTORES = 3;
    /**
     * Número de fragatas con las que comienza la flota.
     */
    private final static int N_FRAGATAS = 4;

    public Flota() {
        this(N_PORTAAVIONES, N_SUBMARINOS, N_DESTRUCTORES, N_FRAGATAS);
    }

    /**
     * Constructora de Flota.
     *
     * @param pPortaaviones
     * @param pSubmarinos
     * @param pDestructores
     * @param pFragatas
     */
    private Flota(int pPortaaviones, int pSubmarinos, int pDestructores,
                  int pFragatas) {
        SimpleBarcoFactory elAstillero = SimpleBarcoFactory.getAstillero();
        this.barcos = new ArrayList<Barco>();
        for (int i = 0; i < pPortaaviones; i++) {
            barcos.add(elAstillero.crearBarco("Portaaviones"));
        }
        for (int i = 0; i < pSubmarinos; i++) {
            barcos.add(elAstillero.crearBarco("Submarino"));
        }
        for (int i = 0; i < pDestructores; i++) {
            barcos.add(elAstillero.crearBarco("Destructor"));
        }
        for (int i = 0; i < pFragatas; i++) {
            barcos.add(elAstillero.crearBarco("Fragata"));
        }
    }

    /**
     * @return Iterador para recorrer la colección de barcos.
     */
    private Iterator<Barco> getIterador() {
        return barcos.iterator();
    }

    /**
     * Recoge las longitudes de los barcos en un array de enteros. Ej.: 1 portaaviones, 2 submarinos, 3
     * destructores, 4 fragatas -> [4, 3, 3, 2, 2, 2, 1, 1, 1, 1].
     * No se garantiza que los enteros sigan ningún orden particular.
     *
     * @return array de enteros con las longitudes los barcos.
     */
    public int[] getLongitudesBarcos() {
        Iterator<Barco> it = getIterador();
        int[] longitudes = new int[barcos.size()];
        int i = 0;
        while (it.hasNext()) {
            longitudes[i] = it.next().getNumSegmentos();
            i++;
        }
        return longitudes;
    }

    /**
     * Comprueba si hay algún barco en pCasilla.
     *
     * @param pCasilla
     * @return true si la casilla indicada esta ocupada por un barco, false en caso contrario.
     */
    public boolean casillaConBarco(Casilla pCasilla) {
        return barcoEnCasilla(pCasilla) != null;
    }

    /**
     * Busca el barco situado en la casilla pCasilla.
     *
     * @param pCasilla
     * @return el barco situado en pCasilla. null si no hay ningún barco en esa casilla o si la posición está
     * fuera del tablero.
     */
    public Barco barcoEnCasilla(Casilla pCasilla) {
        Barco barco = null;
        if (casillaEnTablero(pCasilla)) {
            Iterator<Barco> it = getIterador();
            boolean encontrado = false;
            while (it.hasNext() && !encontrado) {
                barco = it.next();
                encontrado = barco.estaEnCasilla(pCasilla);
            }
            if (!encontrado) {
                barco = null;
            }
        }
        return barco;
    }

    /**
     * Busca los barcos situados en las casillas pCasillas.
     *
     * @param pCasillas
     * @return los barcos situados en la lista de casillas, sin repetir.
     */
    public Collection<Barco> barcosEnCasillas(Collection<Casilla> pCasillas) {
        Collection<Barco> barcos = new ArrayList<>();
        Barco barco;
        if (pCasillas != null) {
            for (Casilla casilla : pCasillas) {
                barco = barcoEnCasilla(casilla);
                if (barco != null && !barcos.contains(barco)) {
                    barcos.add(barco);
                }
            }
        }
        return barcos;
    }

    /**
     * Comprueba si la casilla pCasilla es válida para colocar un barco en ella.
     *
     * @param pCasilla
     * @return true si la casilla indicada puede ser ocupada por un Barco.
     */
    public boolean casillaColocable(Casilla pCasilla) {
        if (casillaEnTablero(pCasilla)) {
            boolean ocupada = false;
            Casilla casilla = pCasilla.getLocation();
            // casilla objetivo
            ocupada = casillaConBarco(casilla);
            if (!ocupada) {
                casilla.translate(-1, 0);
                // casilla de la izquierda
                ocupada = casillaConBarco(casilla);
            }
            if (!ocupada) {
                casilla.translate(0, -1);
                // casilla de la esquina superior izquierda
                ocupada = casillaConBarco(casilla);
            }
            if (!ocupada) {
                casilla.translate(1, 0);
                // casilla de arriba
                ocupada = casillaConBarco(casilla);
            }
            if (!ocupada) {
                casilla.translate(1, 0);
                // casilla de la esquina superior derecha
                ocupada = casillaConBarco(casilla);
            }
            if (!ocupada) {
                casilla.translate(0, 1);
                // casilla de la derecha
                ocupada = casillaConBarco(casilla);
            }
            if (!ocupada) {
                casilla.translate(0, 1);
                // casilla de la esquina inferior derecha
                ocupada = casillaConBarco(casilla);
            }
            if (!ocupada) {
                casilla.translate(-1, 0);
                // casilla de abajo
                ocupada = casillaConBarco(casilla);
            }
            if (!ocupada) {
                casilla.translate(-1, 0);
                // casilla de la esquina inferior izquierda
                ocupada = casillaConBarco(casilla);
            }
            return !ocupada;
        } else {
            return false;
        }
    }

    /**
     * Comprueba si las casillas de la colección pCasillas son aptas para colocar un barco en ellas.
     *
     * @param pCasillas
     * @return true si todas las casillas de pCasillas pueden alojar un segmento de barco.
     */
    public boolean casillasColocables(Collection<Casilla> pCasillas) {
        boolean correcto = true;
        if (pCasillas != null) {
            Iterator<Casilla> it = pCasillas.iterator();
            while (it.hasNext() && correcto) {
                correcto = casillaColocable(it.next());
            }
        } else {
            correcto = false;
        }
        return correcto;
    }

    /**
     * Recoge todos los barcos de la flota, sacándolos de sus posiciones.
     */
    public void recogerBarcos() {
        Iterator<Barco> it = getIterador();
        while (it.hasNext()) {
            it.next().recoger();
        }
        setChanged();
        notifyObservers(new NotifyArg("recogerBarcos", null));
    }

    /**
     * Coloca un barco del tamaño apropiado en las casillas dadas si estas son válidas.
     *
     * @param pCasillas
     * @return false si las casillas no son válidas, el barco no se puede colocar o no hay ningún barco apropiado disponible. true si no
     * surge ningún prblema.
     */
    public boolean colocarBarco(Collection<Casilla> pCasillas) {
        boolean colocado = false;
        if (casillasColocables(pCasillas)) {
            Iterator<Barco> it = getIterador();
            Barco barco;
            while (it.hasNext() && !colocado) {
                barco = it.next();
                if (barco.getNumSegmentos() == pCasillas.size() && !barco.estaColocado()) {
                    colocado = barco.colocar(pCasillas);
                    setChanged();
                    notifyObservers(new NotifyArg("colocarBarco", pCasillas));
                }
            }
        }
        return colocado;
    }

    /**
     * @param pLongitud
     * @return número de barcos con pLongitud segmentos que faltan por colocar
     */
    public int restantes(int pLongitud) {
        int n = 0;
        Iterator<Barco> it = getIterador();
        Barco barco;
        while (it.hasNext()) {
            barco = it.next();
            if (barco.getNumSegmentos() == pLongitud && !barco.estaColocado()) {
                n++;
            }
        }
        return n;
    }

    /**
     * @param pCasilla
     * @return el contenido de la casilla (AGUA, ESCUDO, DANADO o BARCO).
     */
    public ContenidoCasilla getContenido(Casilla pCasilla) {
        ContenidoCasilla contenido;
        Barco barco = barcoEnCasilla(pCasilla);
        if (barco != null) {
            contenido = barco.getEstado(pCasilla);
        } else {
            contenido = ContenidoCasilla.AGUA;
        }
        return contenido;
    }

    /**
     * Comprueba si pCasilla está dentro de los límites del tablero.
     *
     * @param pCasilla
     * @return true si la casilla dada está dentro del tablero, false en caso contrario.
     */
    public static boolean casillaEnTablero(Casilla pCasilla) {
        return pCasilla.getX() >= 0 && pCasilla.getX() < Juego.ALTURA_TABLERO && pCasilla.getY() >= 0
                       && pCasilla.getY() < Juego.ANCHURA_TABLERO;
    }

    /**
     * Calcula la colección de casillas representada por los parámetros.
     *
     * @param pCasilla     la casilla de origen.
     * @param pOrientacion la dirección en la que coger el resto de casillas.
     * @param pNum         el número de casillas a coger (contando el origen).
     * @return la colección de casillas. null si alguna de las casillas quedan fuera del tablero.
     */
    public static Collection<Casilla> encontrarCasillas(Casilla pCasilla,
                                                        Orientacion pOrientacion, int pNum) {
        Collection<Casilla> casillas = null;
        if (casillaEnTablero(pCasilla)) {
            Casilla casillaAux = pCasilla.getLocation();
            casillas = new ArrayList<Casilla>(pNum);
            casillas.add(casillaAux.getLocation());
            for (int i = 0; i < pNum - 1; i++) {
                switch (pOrientacion) {
                    case NORTE:
                        casillaAux.translate(-1, 0);
                        break;
                    case SUR:
                        casillaAux.translate(1, 0);
                        break;
                    case ESTE:
                        casillaAux.translate(0, 1);
                        break;
                    case OESTE:
                        casillaAux.translate(0, -1);
                        break;
                }
                if (casillaEnTablero(casillaAux)) {
                    casillas.add(casillaAux.getLocation());
                } else {
                    casillas = null;
                    break;
                }
            }
        }
        return casillas;
    }

    /**
     * Recibe un disparo en todas las casillas de pCasillas.
     * Más concretamente, ejecuta pConsumer para cada barco situado en alguna de las casillas de pCasillas, sin repetir.
     *
     * @param pCasillas
     * @param pConsumer
     */
    public void recibirDisparo(Collection<Casilla> pCasillas, Consumer<Barco> pConsumer) {
        Collection<Barco> barcos = barcosEnCasillas(pCasillas);
        barcos.stream().forEach(pConsumer);
    }

    // Los métodos que siguien son métodos públicos que hacen llamadas a los métodos privados. Su ojetivo es
    // ser usados en los JUnit, no en la aplicación final.

    /**
     * Dibuja un tablero con los barcos de la flota del jugador.
     */
    public String returnTablero() {
        String tablero = "";
        System.out.println();
        for (int fila = 0; fila < Juego.ALTURA_TABLERO; fila++) {
            for (int col = 0; col < Juego.ANCHURA_TABLERO; col++) {
                tablero += " "
                                   + getCharBarco(barcoEnCasilla(new Casilla(fila, col)), new Casilla(fila, col));
            }
            tablero += "\n";
        }
        return tablero;

    }

    /**
     * Devuelve un caracter como representación de un barco.
     *
     * @param pBarco
     * @return
     */
    private char getCharBarco(Barco pBarco, Casilla pCasilla) {
        char agua = '~'; // agua
        char res = agua;
        if (pBarco != null) {
            res = pBarco.getCharBarco(pCasilla);
        }
        return res;
    }

    public void colocacionControlada() {
        recogerBarcos();
        Iterator<Barco> it = getIterador();
        ArrayList<Casilla> casillas = new ArrayList<Casilla>(4);
        // Portaaviones
        casillas.add(new Casilla(0, 0));
        casillas.add(new Casilla(1, 0));
        casillas.add(new Casilla(2, 0));
        casillas.add(new Casilla(3, 0));
        it.next().colocar(casillas);
        setChanged();
        notifyObservers(new NotifyArg("colocarBarco", casillas));
        casillas.clear();
        // Submarino 1
        casillas.add(new Casilla(0, 2));
        casillas.add(new Casilla(1, 2));
        casillas.add(new Casilla(2, 2));
        it.next().colocar(casillas);
        setChanged();
        notifyObservers(new NotifyArg("colocarBarco", casillas));
        casillas.clear();
        // Submarino 2
        casillas.add(new Casilla(4, 2));
        casillas.add(new Casilla(4, 3));
        casillas.add(new Casilla(4, 4));
        it.next().colocar(casillas);
        setChanged();
        notifyObservers(new NotifyArg("colocarBarco", casillas));
        casillas.clear();
        // Destructor 1
        casillas.add(new Casilla(9, 9));
        casillas.add(new Casilla(9, 8));
        it.next().colocar(casillas);
        setChanged();
        notifyObservers(new NotifyArg("colocarBarco", casillas));
        casillas.clear();
        // Destructor 2
        casillas.add(new Casilla(0, 9));
        casillas.add(new Casilla(0, 8));
        it.next().colocar(casillas);
        setChanged();
        notifyObservers(new NotifyArg("colocarBarco", casillas));
        casillas.clear();
        // Destructor 3
        casillas.add(new Casilla(9, 0));
        casillas.add(new Casilla(8, 0));
        it.next().colocar(casillas);
        setChanged();
        notifyObservers(new NotifyArg("colocarBarco", casillas));
        casillas.clear();
        // Fragata 1
        casillas.add(new Casilla(4, 6));
        it.next().colocar(casillas);
        setChanged();
        notifyObservers(new NotifyArg("colocarBarco", casillas));
        casillas.clear();
        // Fragata 2
        casillas.add(new Casilla(0, 6));
        it.next().colocar(casillas);
        setChanged();
        notifyObservers(new NotifyArg("colocarBarco", casillas));
        casillas.clear();
        // Fragata 3
        casillas.add(new Casilla(6, 0));
        it.next().colocar(casillas);
        setChanged();
        notifyObservers(new NotifyArg("colocarBarco", casillas));
        casillas.clear();
        // Fragata 4
        casillas.add(new Casilla(6, 4));
        it.next().colocar(casillas);
        setChanged();
        notifyObservers(new NotifyArg("colocarBarco", casillas));
        casillas.clear();
    }

    /**
     * @return true si todos los barcos de la flota han sido destruidos. false en caso contrario.
     */
    public boolean estaDestruida() {
        boolean destruido = true;
        Iterator<Barco> it = getIterador();
        while (it.hasNext() && destruido) {
            destruido = it.next().estaDestruido();
        }
        return destruido;
    }

    /**
     * Devuelve una colección de barcos
     * @return
     */
    public Collection<Barco> getBarcos() {
        return barcos;
    }


}
