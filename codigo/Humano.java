package codigo;

import java.util.Collection;

import codigo.cpu.CPU;

public class Humano extends Jugador {

    public Humano(String pNombre) {
        super(pNombre);
    }

    public Humano() {
        this("no-face");
    }

    /**
     * Coloca los barcos de la Flota del jugador en sus respectivas posiciones.
     *
     * @return true si la operación se ha llevado a cabo sin errores. false en caso contrario.
     */
    @Override
    public boolean colocarBarcos() {
        this.setChanged();
        this.notifyObservers(new NotifyArg("colocarBarcos", null));
        return true;
    }

    @Override
    public boolean repararBarco(Casilla pCasilla) {
        boolean exito = super.repararBarco(pCasilla);
        if (exito) {
            CPU cpu = (CPU) Juego.getElJuego().getJ2();
            Collection<Casilla> casillas = getFlota().barcoEnCasilla(pCasilla).getCasillas();
            for (Casilla casilla : casillas) {
                cpu.addCasillaSinRepetir(casilla, cpu.getPosicionesDisparables());
            }
        }
        return exito;
    }
}
