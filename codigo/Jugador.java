package codigo;

import codigo.excepciones.ArmaNoEquipadaException;
import codigo.excepciones.NotEnoughCashException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Observable;

/**
 * Esta clase representa a un jugador y nunca debería ser instanciada (abstracta). Para ello se deben utilizar
 * sus subclases Humano y cpu. Jugador almacena su propia flota, la flota del oponente, un arsenal y una lista
 * de los disparos realizados.
 */
public abstract class Jugador extends Observable {

    private String nombre;
    /**
     * La Flota del jugador.
     */
    private Flota flota;
    /**
     * La Flota del oponente.
     */
    private Flota flotaOponente;
    /**
     * El Arsenal del jugador.
     */
    private Arsenal arsenal;
    /**
     * La lista de disparos realizados por el jugador hasta el momento.
     */
    private Collection<Casilla> listaDisparos;
    /**
     * Este flag indica si el jugador ha sido inicializado o no, para impedir que el jugador sea inicializado
     * múltiples veces.
     */
    private boolean inicializado;

    /**
     * Este flag indica si es el turno del jugador y, por tanto, puede actuar o no.
     */
    private boolean turnoActivo;
    /**
     * Este flag indidica si el jugador ha disparado este turno
     */
    private boolean disparado;
    /**
     * Este flag indidica si el jugador ha realizado una acción especial que solo puede realizarse una vez por
     * turno.
     */
    private boolean accionEspecial;

    public Jugador(String pNombre) {
        nombre = pNombre;
        flota = new Flota();
        arsenal = new Arsenal();
        listaDisparos = new ArrayList<Casilla>();
        inicializado = false;
        turnoActivo = false;
        disparado = false;
        accionEspecial = false;
    }

    public String getNombre() {
        return nombre;
    }

    /**
     * Devuelve la flota del oponente.
     */
    public Flota getFlotaOponente() {
        return flotaOponente;
    }

    /**
     * Devuelve la flota del jugador.
     */
    public Flota getFlota() {
        return flota;
    }

    /**
     * Devuelve el arsenal del jugador.
     */
    public Arsenal getArsenal() {
        return arsenal;
    }

    /**
     * Devuelve la lista de disparos realizados.
     */
    public Collection<Casilla> getListaDisparos() {
        return listaDisparos;
    }

    /**
     * Inicializa el jugador y todos sus atributos, asigna la flota del oponente, coloca los barcos de la
     * flota propia y activa el flag de inicializado.
     *
     * @param pFlotaOponente
     */
    public void inicializar(Flota pFlotaOponente) {
        if (!inicializado) {
            asignarFlotaOponente(pFlotaOponente);
            inicializado = colocarBarcos();
        }
    }

    /**
     * Asigna pFlotaOponente como la flota del oponente.
     *
     * @param pFlotaOponente
     */
    private void asignarFlotaOponente(Flota pFlotaOponente) {
        flotaOponente = pFlotaOponente;
    }

    /**
     * Coloca los barcos de la flota del jugador en sus respectivas posiciones. Este método no esta
     * implementado, sino que se reescribe en las sublcases Humano y cpu ya que su funcionamiento es distinto
     * en cada una.
     *
     * @return true si la operación se ha llevado a cabo sin errores, false en caso contrario.
     */
    public boolean colocarBarcos() {
        throw new UnsupportedOperationException();
    }

    /**
     * Equipa el arma de identificador pIdArma.
     *
     * @param pIdArma
     */
    public void equiparArma(int pIdArma) {
        if (arsenal.equiparArma(pIdArma)) {
            setChanged();
            notifyObservers(new NotifyArg("equiparArma", arsenal.getArmaEquipada()));
        }
    }

    /**
     * Ataca con el arma equipada a la casilla pCasilla del tablero enemigo.
     *
     * @param pCasilla
     * @return la colección de casillas alcanzadas por el disparo.
     */
    public Collection<Casilla> atacar(Casilla pCasilla) {
        if (turnoActivo && !disparado) {
            try {
                Collection<Casilla> casillas = arsenal.disparar(flotaOponente, pCasilla);
                if (disparado = casillas != null) {
                    setChanged();
                    notifyObservers(new NotifyArg("atacar", pCasilla));
                }
                return casillas;
            } catch (ArmaNoEquipadaException e) {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Repara el barco situado en la casilla pCasilla del tablero del jugador.
     *
     * @param pCasilla
     * @return true si el barco se ha reparado exitosamente, false en caso contrario.
     */
    public boolean repararBarco(Casilla pCasilla) {
        if (turnoActivo && !accionEspecial) {
            try {
                if (accionEspecial = arsenal.repararBarco(flota, pCasilla)) {
                    setChanged();
                    notifyObservers(new NotifyArg("repararBarco", pCasilla));
                }
                return accionEspecial;
            } catch (NotEnoughCashException e) {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Activa el escudo del barco situado en la casilla pCasilla del tablero del jugador.
     *
     * @param pCasilla
     * @return true si el escudo se ha activado exitosamente, false en caso contrario.
     */
    public boolean activarEscudo(Casilla pCasilla) {
        if (turnoActivo && !accionEspecial) {
            try {
                if (accionEspecial = arsenal.activarEscudo(flota, pCasilla)) {
                    setChanged();
                    notifyObservers(new NotifyArg("activarEscudo", pCasilla));
                }
                return accionEspecial;
            } catch (NotEnoughCashException e) {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Recoge todos los barcos de la flota, sacándolos de sus posiciones.
     */
    public void recogerBarcos() {
        getFlota().recogerBarcos();
    }

    /**
     * @return true si todos los barcos del jugador han sido destruidos. false en caso contrario.
     */
    public boolean haPerdido() {
        return flota.estaDestruida();
    }

    /**
     * Comienza el turno del jugador.
     */
    public void comenzarTurno() {
        if (!turnoActivo) {
            turnoActivo = true;
            disparado = false;
            accionEspecial = false;
        }
    }

    /**
     * Termina el turno del jugador.
     */
    public void terminarTurno() {
        if (turnoActivo && disparado) {
            turnoActivo = false;
            Juego.getElJuego().cambiarTurno();
        }
    }

    // Para los Unit Tests {{{
    public void setTurnoActivo(boolean turnoActivo) {
        this.turnoActivo = turnoActivo;
    }

    public void setDisparado(boolean disparado) {
        this.disparado = disparado;
    }

    public void setAccionEspecial(boolean accionEspecial) {
        this.accionEspecial = accionEspecial;
    }

    // Para los Unit Tests }}}
}
