package codigo.barcos;

public class Portaaviones extends Barco {

    private static int NUM_SEGMENTOS = 4;

    public Portaaviones() {
        super(NUM_SEGMENTOS);
    }

    @Override
    public char getCharNombre() {
        return 'P';
    }

}
