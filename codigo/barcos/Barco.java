package codigo.barcos;

import codigo.Casilla;
import codigo.Flota;
import codigo.ContenidoCasilla;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Esta clase representa un barco. Tiene acceso a la colección de segmentos que conforman el barco, y a la
 * vida de su escudo. Esta clase no debe instanciarse (abstracta) sino que deben utilizarse sus subclases.
 */
public abstract class Barco {

    /**
     * Máxima vida que puede tener el escudo.
     */
    private final int MAX_VIDA_ESCUDO = 2;
    /**
     * Número de Segmentos que conforman el barco.
     */
    private int numSegmentos;
    /**
     * Colección de Segmentos que conforman el barco.
     */
    private Collection<Segmento> segmentos;
    /**
     * Vida restante del escudo.
     */
    private int vidaEscudo;
    /**
     * Este flag indica si el barco ha sudo totalmente destruido (hundido). Un barco hundido no puede ser
     * reparado.
     */
    private boolean destruido;
    /**
     * Este flag indica si el barco ha sido colocado en el tablero con éxito para evitar que se pueda
     * recolocar un barco que ya ha sido colocado.
     */
    private boolean colocado;

    public Barco(int pNumSegmentos) {
        vidaEscudo = 0;
        destruido = false;
        colocado = false;
        numSegmentos = pNumSegmentos;
        segmentos = new ArrayList<Segmento>(numSegmentos);
        for (int i = 0; i < numSegmentos; i++) {
            segmentos.add(new Segmento());
        }
    }

    public int getNumSegmentos() {
        return numSegmentos;
    }

    public boolean estaDestruido() {
        return destruido;
    }

    public boolean estaColocado() {
        return colocado;
    }

    /**
     * Devuelve la lista de casillas que ocupa el barco.
     */
    public Collection<Casilla> getCasillas() {
        ArrayList<Casilla> casillas = new ArrayList<Casilla>(numSegmentos);
        for (Segmento segmento : segmentos) {
            casillas.add(segmento.getCasilla().getLocation());
        }
        return casillas;
    }

    /**
     * Recibe un disparo en el segmento en pCasilla. Si el escudo está activado lo daña según pDano. Si no,
     * daña el segmento.
     *
     * @param pDano
     * @param pCasilla
     */
    public void recibirDisparo(int pDano, Casilla pCasilla) {
        if (vidaEscudo > 0) {
            danarEscudo(pDano);
        } else {
            Segmento segmento = segmentoEnCasilla(pCasilla);
            if (segmento != null) {
                segmento.danar();
                if (segmetosDanados() == numSegmentos) {
                    destruido = true;
                }
            }
        }
    }

    /**
     * Recibe un impacto en el barco completo. Si el escudo esta activo lo destruye. Si no, destruye el barco
     * entero.
     */
    public void recibirImpacto() {
        if (vidaEscudo > 0) {
            destruirEscudo();
        } else {
            Iterator<Segmento> it = getIterador();
            while (it.hasNext()) {
                it.next().danar();
            }
            destruido = true;
        }
    }

    /**
     * Reduce la vida del escudo en la cantidad indicada.
     *
     * @param pDano
     */
    private void danarEscudo(int pDano) {
        if ((vidaEscudo = vidaEscudo - pDano) < 0)
            vidaEscudo = 0;
    }

    /**
     * Reduce la vida del escudo a 0.
     */
    private void destruirEscudo() {
        vidaEscudo = 0;
    }

    /**
     * Aumenta la vida del escudo al máximo posible. Si el escudo ya estaba activo, incluso dañado, no hace
     * nada.
     *
     * @return true si el escudo es activado, false en caso contrario.
     */
    public boolean activarEscudo() {
        if (vidaEscudo == 0 && !destruido) {
            vidaEscudo = MAX_VIDA_ESCUDO;
            return true;
        } else
            return false;
    }

    /**
     * Busca la cantidad de segmentos dañados del barco.
     *
     * @return el número de segmentos dañados.
     */
    public int segmetosDanados() {
        int num = 0;
        Iterator<Segmento> it = getIterador();
        while (it.hasNext()) {
            if (it.next().estaDanado())
                num++;
        }
        return num;
    }

    /**
     * Comprueba en todos los segmentos si alguno de ellos está dañado.
     *
     * @return true si alguno de los segmentos está dañado, false en caso contrario.
     */
    public boolean estaDanado() {
        boolean danado = false;
        Iterator<Segmento> it = getIterador();
        while (it.hasNext() && !danado) {
            danado = it.next().estaDanado();
        }
        return danado;
    }

    /**
     * Repara todos los Segmentos dañados del Barco. Tiene que haber por lo menos un segmento dañado y el baro
     * no puede haber sido destruido por completo.
     *
     * @return true si la reparación se lleva a cabo con éxito.
     */
    public boolean reparar() {
        Iterator<Segmento> it = getIterador();
        if (estaDanado() && !destruido) {
            while (it.hasNext()) {
                it.next().reparar();
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Comprueba si alguno de los segmentos está colocado en pCasilla.
     *
     * @param pCasilla
     * @return true si el barco ocupa la casilla, false en caso contrario.
     */
    public boolean estaEnCasilla(Casilla pCasilla) {
        return segmentoEnCasilla(pCasilla) != null;
    }

    /**
     * Busca el segmento en la casilla pCasilla.
     *
     * @param pCasilla
     * @return el segmento colocado en la casilla, null si no hay ningún segmento en la casilla o si la
     * casilla está fuera del tablero.
     */
    private Segmento segmentoEnCasilla(Casilla pCasilla) {
        Segmento segmento = null;
        if (Flota.casillaEnTablero(pCasilla)) {
            Iterator<Segmento> it = getIterador();
            boolean encontrado = false;
            while (it.hasNext() && !encontrado) {
                segmento = it.next();
                encontrado = segmento.estaEnCasilla(pCasilla);
            }
            if (!encontrado) {
                segmento = null;
            }
        }
        return segmento;
    }

    /**
     * Si el barco no ha sido colocado ya, coloca sus segmentos en la serie de casillas indicadas. No se
     * comprueba si las casillas son válidas.
     *
     * @param pCasillas
     * @return true si la operación se ha llevado a cabo sin errores. En caso contrario, devuelve false y
     * deshace los cambios.
     */
    public boolean colocar(Collection<Casilla> pCasillas) {
        Iterator<Segmento> itSegmentos = getIterador();
        Iterator<Casilla> itCasillas = pCasillas.iterator();
        boolean correcto = true;
        if (numSegmentos == pCasillas.size()) {
            while (itSegmentos.hasNext() && itCasillas.hasNext() && correcto) {
                correcto = itSegmentos.next().colocar(itCasillas.next());
            }
            if (itSegmentos.hasNext() != itCasillas.hasNext() || !correcto) {
                correcto = false;
                recoger();
            } else {
                colocado = true;
            }
        }
        return correcto;
    }

    /**
     * Recoge todos los segmentos de sus casillas y cambia el estado del flag para indicar que el barco no
     * esta colocado.
     */
    public void recoger() {
        Iterator<Segmento> it = getIterador();
        while (it.hasNext()) {
            it.next().recoger();
        }
        colocado = false;
    }

    /**
     * @return Iterador para recorrer la colección de segmentos.
     */
    private Iterator<Segmento> getIterador() {
        return segmentos.iterator();
    }

    /**
     *
     * @param pCasilla
     * @return el estado del barco (ESCUDO, DANADO o BARCO).
     */
    public ContenidoCasilla getEstado(Casilla pCasilla) {
        ContenidoCasilla estado;
        if (vidaEscudo > 0) {
            estado = ContenidoCasilla.ESCUDO;
        } else if (segmentoEnCasilla(pCasilla).estaDanado()) {
            estado = ContenidoCasilla.DANADO;
        } else {
            estado = ContenidoCasilla.BARCO;
        }
        return estado;
    }

    // Los métodos que siguien son métodos públicos que hacen llamadas a los métodos privados. Su ojetivo es
    // ser usados en los JUnit, no en la aplicación final.

    public char getCharBarco(Casilla pCasilla) {
        char agua = '~'; // agua
        char barco = 'B'; // barco sin escudo
        char danado = '_'; // barco dañado sin escudo
        char escudo = 'E'; // escudo max vida (sin importar el estado del barco)
        char escudoDanado = 'e'; // escudo dañado (sin importar el estado del barco)
        char res = agua;
        if (vidaEscudo == MAX_VIDA_ESCUDO) {
            res = escudo;
        } else if (vidaEscudo > 0) {
            res = escudoDanado;
        } else if (segmentoEnCasilla(pCasilla).estaDanado()) {
            res = danado;
        } else {
            res = barco;
        }
        return res;
    }

    /**
     * @return el primer caracter del nombre del barco.
     */
    public char getCharNombre() {
        return ' ';
    }

    /**
     * Comprueba el estado del escudo.
     * @return true si está escudado, false si no lo está
     */
    public boolean estaEscudado() {
        return this.vidaEscudo > 0;
    }
}
