package codigo.barcos;

public class Fragata extends Barco {

    private static int NUM_SEGMENTOS = 1;

    public Fragata() {
        super(NUM_SEGMENTOS);
    }

    @Override
    public char getCharNombre() {
        return 'F';
    }

}
