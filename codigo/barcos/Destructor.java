package codigo.barcos;

public class Destructor extends Barco {

    private static int NUM_SEGMENTOS = 2;

    public Destructor() {
        super(NUM_SEGMENTOS);
    }

    @Override
    public char getCharNombre() {
        return 'D';
    }

}
