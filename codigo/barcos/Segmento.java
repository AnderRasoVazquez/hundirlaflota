package codigo.barcos;

import codigo.Casilla;

/**
 * Esta clase representa un segmento de un barco. Almacena información acerca de su estado y de en que
 * posición está colocado.
 */
public class Segmento {

    /**
     * Indica si el segmento ha sido alcanzado.
     */
    private boolean danado = false;
    /**
     * CasillaInterfaz en la que está colocado.
     */
    private Casilla casilla;
    /**
     * Este flag indica si el segmento ha sido colocado, para evitar que pueda ser colocado de nuevo.
     */
    private boolean colocado = false;

    public Segmento() {

    }

    /**
     * Devuelve el estado del segmento.
     */
    public boolean estaDanado() {
        return danado;
    }

    /**
     * Devuelve la casilla que ocupa el segmento.
     */
    public Casilla getCasilla() {
        return casilla;
    }
    
    /**
     * Cambia el estado del Segmento a dañado.
     */
    public void danar() {
        danado = true;
    }

    /**
     * Cambia el estado del Segmento a no dañado.
     */
    public void reparar() {
        danado = false;
    }

    /**
     * Comprueba si está colocado en pCasilla.
     *
     * @param pCasilla
     * 
     * @return true si está colocado en la casilla.
     */
    public boolean estaEnCasilla(Casilla pCasilla) {
        return pCasilla.equals(casilla);
    }

    /**
     * Si el segmento no ha sido colocado, lo coloca en pCasilla. No se comprueba si la casilla es válida.
     *
     * @param pCasilla
     * 
     * @return true si es colocado con exito, false en caso contrario.
     */
    public boolean colocar(Casilla pCasilla) {
        if (!colocado) {
            casilla = pCasilla;
            colocado = true;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Elimina la información acerca de en que casilla está colocado el segmento.
     */
    public void recoger() {
        colocado = false;
        casilla = null;
    }

}
