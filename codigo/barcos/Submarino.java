package codigo.barcos;

public class Submarino extends Barco {

    private static int NUM_SEGMENTOS = 3;

    public Submarino() {
        super(NUM_SEGMENTOS);
    }

    @Override
    public char getCharNombre() {
        return 'S';
    }

}
