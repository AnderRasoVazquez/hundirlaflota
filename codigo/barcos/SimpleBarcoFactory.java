package codigo.barcos;

/**
 * Esta clase se encarga de la creación de nuevos barcos.
 */
public class SimpleBarcoFactory {

    private static SimpleBarcoFactory ElAstillero;

    private SimpleBarcoFactory() {

    }

    public static SimpleBarcoFactory getAstillero() {
        if (ElAstillero == null)
            ElAstillero = new SimpleBarcoFactory();
        return ElAstillero;
    }

    /**
     * Crea una nueva instancia de la subclase de barco indicada por pTipo.
     * 
     * @param pTipo
     * 
     * @return la instancia creada.
     */
    public Barco crearBarco(String pTipo) {
        Barco miBarco = null;
        switch (pTipo.toLowerCase()) {
        case "portaaviones":
            miBarco = new Portaaviones();
            break;
        case "submarino":
            miBarco = new Submarino();
            break;
        case "destructor":
            miBarco = new Destructor();
            break;
        case "fragata":
            miBarco = new Fragata();
            break;
        default:
            // throw new ValueException("Escribe un nombre válido para un barco");
            break;
        }
        return miBarco;
    }
}
