package codigo.armas;

import java.util.ArrayList;
import java.util.Collection;

import codigo.Casilla;
import codigo.Flota;

/**
 * Esta clase representa un grupo de bombas.
 */
public class Bombas extends Armamento {

    /**
     * Identifcador númerico de las bombas. id=0.
     */
    public final static int ID_BOMBAS = 0;
    /**
     * Precio de una bomba.
     */
    public final static int PRECIO_BOMBA = 0;
    /**
     * Munición inicial de bombas con las que se comienza el juego.
     */
    public static int MUNICION_INICIAL_BOMBAS = 1;
    /**
     * Daño que causa una bomba en un escudo.
     */
    private static int DANO_BOMBAS = 1;

    public Bombas() {
        super(PRECIO_BOMBA, MUNICION_INICIAL_BOMBAS);
    }

    /**
     * Dispara una bomba a pCasilla de pFlota.
     * 
     * @param pFlota
     * @param pCasilla
     * 
     * @return la lista de posiciones que han sido alcanzadas y, por tanto, reveladas.
     */
    @Override
    public Collection<Casilla> disparar(Flota pFlota, Casilla pCasilla) {
//        Collection<Casilla> casillas = null;
//        if (puedeDisparar() && Flota.casillaEnTablero(pCasilla)) {
//            casillas = new ArrayList<Casilla>(1);
//            Barco barco = pFlota.barcoEnCasilla(pCasilla);
//            if (barco != null) {
//                barco.recibirDisparo(DANO_BOMBAS, pCasilla);
//            }
//            casillas.add(pCasilla.getLocation());
//        }
//        return casillas;
        Collection<Casilla> casillas = null;
        if (puedeDisparar() && Flota.casillaEnTablero(pCasilla)) {
            casillas = new ArrayList<>(1);
            casillas.add(pCasilla.getLocation());
            pFlota.recibirDisparo(casillas, b -> b.recibirDisparo(Bombas.DANO_BOMBAS, pCasilla));
        }
        return casillas;
    }

    /**
     * Devuelve el identificador de las bombas.
     */
    @Override
    public int getId() {
        return ID_BOMBAS;
    }

    @Override
    public String getNombre() {
        return "bombas";
    }

    /**
     * Este tipo de Armamento no se puede abastecer.
     * 
     * @param pCantidad
     *
     * @return la cantidad de munición que queda despues de la operación.
     */
    @Override
    public int abastecer(int pCantidad) {
        return super.abastecer(0);
    }

}
