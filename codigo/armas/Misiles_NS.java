package codigo.armas;

import java.util.Collection;

import codigo.Casilla;
import codigo.Flota;
import codigo.Juego;
import codigo.Orientacion;

/**
 * Esta clase representa un grupo de misiles_NS.
 */
public class Misiles_NS extends Misiles {

    /**
     * Identifcador númerico de los misiles_NS. id=2.
     */
    public final static int ID_MISILES_NS = 2;
    /**
     * Precio de un misil_NS.
     */
    public final static int PRECIO_MISIL_NS = 400;
    /**
     * Munición inicial de misiles_NS con los que se comienza el juego.
     */
    public static int MUNICION_INICIAL_MISILES_NS = 1;

    public Misiles_NS() {
        super(PRECIO_MISIL_NS, MUNICION_INICIAL_MISILES_NS);
    }

    /**
     * Dispara un misil_NS a pCasilla de pFlota.
     * 
     * @param pFlota
     * @param pCasilla
     * 
     * @return la lista de posiciones que han sido alcanzadas y, por tanto, reveladas.
     */
    @Override
    public Collection<Casilla> disparar(Flota pFlota, Casilla pCasilla) {
//        Collection<Casilla> casillas = null;
//        if (puedeDisparar() && Flota.casillaEnTablero(pCasilla)) {
//            abastecer(-1);
//            casillas = new ArrayList<Casilla>(Juego.ALTURA_TABLERO);
//            Collection<Barco> barcos = new ArrayList<Barco>();
//            Barco barco;
//            Casilla casilla = pCasilla.getLocation();
//            casilla.move(0, (int) casilla.getY());
//            while (Flota.casillaEnTablero(casilla)) {
//                barco = pFlota.barcoEnCasilla(casilla);
//                if (barco != null && !barcos.contains(barco)) {
//                    barco.recibirImpacto();
//                    barcos.add(pFlota.barcoEnCasilla(casilla));
//                }
//                casillas.add(casilla.getLocation());
//                casilla.translate(1, 0);
//            }
//        }
//        return casillas;
        Collection<Casilla> casillas = null;
        if (puedeDisparar()) {
            abastecer(-1);
            casillas = Flota.encontrarCasillas(new Casilla(0, (int) pCasilla.getY()), Orientacion.SUR, Juego.ALTURA_TABLERO);
            pFlota.recibirDisparo(casillas, b -> b.recibirImpacto());
        }
        return casillas;
    }

    /**
     * Devuelve el identificador de los misiles_NS.
     */
    @Override
    public int getId() {
        return ID_MISILES_NS;
    }

    @Override
    public String getNombre() {
        return "misiles Norte-Sur";
    }

}
