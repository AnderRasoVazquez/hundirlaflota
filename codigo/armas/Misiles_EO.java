package codigo.armas;

import java.util.Collection;

import codigo.Casilla;
import codigo.Flota;
import codigo.Juego;
import codigo.Orientacion;

/**
 * Esta clase representa un grupo de misiles_EO.
 */
public class Misiles_EO extends Misiles {

    /**
     * Identifcador númerico de los misiles_EO. id=3.
     */
    public final static int ID_MISILES_EO = 3;
    /**
     * Precio de un misil_EO.
     */
    public final static int PRECIO_MISIL_EO = 400;
    /**
     * Munición inicial de misiles_EO con los que se comienza el juego.
     */
    public static int MUNICION_INICIAL_MISILES_EO = 1;

    public Misiles_EO() {
        super(PRECIO_MISIL_EO, MUNICION_INICIAL_MISILES_EO);
    }

    /**
     * Dispara un misil_EO a pCasilla de pFlota.
     * 
     * @param pFlota
     * @param pCasilla
     * 
     * @return la lista de posiciones que han sido alcanzadas y, por tanto, reveladas.
     */
    @Override
    public Collection<Casilla> disparar(Flota pFlota, Casilla pCasilla) {
//        Collection<Casilla> casillas = null;
//        if (puedeDisparar() && Flota.casillaEnTablero(pCasilla)) {
//            abastecer(-1);
//            casillas = new ArrayList<Casilla>(Juego.ANCHURA_TABLERO);
//            Collection<Barco> barcos = new ArrayList<Barco>();
//            Barco barco;
//            Casilla casilla = pCasilla.getLocation();
//            casilla.move((int) casilla.getX(), 0);
//            while (Flota.casillaEnTablero(casilla)) {
//                barco = pFlota.barcoEnCasilla(casilla);
//                if (barco != null && !barcos.contains(barco)) {
//                    barco.recibirImpacto();
//                    barcos.add(pFlota.barcoEnCasilla(casilla));
//                }
//                casillas.add(casilla.getLocation());
//                casilla.translate(0, 1);
//            }
//        }
//        return casillas;
        Collection<Casilla> casillas = null;
        if (puedeDisparar()) {
            abastecer(-1);
            casillas = Flota.encontrarCasillas(new Casilla((int) pCasilla.getX(), 0), Orientacion.ESTE, Juego.ALTURA_TABLERO);
            pFlota.recibirDisparo(casillas, b -> b.recibirImpacto());
        }
        return casillas;
    }

    /**
     * Devuelve el identificador de los misiles_EO.
     */
    @Override
    public int getId() {
        return ID_MISILES_EO;
    }

    @Override
    public String getNombre() {
        return "misiles Este-Oeste";
    }

}
