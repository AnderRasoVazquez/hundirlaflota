package codigo.armas;

import java.util.ArrayList;
import java.util.Collection;

import codigo.Casilla;
import codigo.Flota;

/**
 * Esta clase representa un grupo de misiles.
 */
public class Misiles extends Armamento {

    /**
     * Identifcador númerico de los misiles. id=1.
     */
    public final static int ID_MISILES = 1;
    /**
     * Precio de un misil.
     */
    public final static int PRECIO_MISIL = 100;
    /**
     * Munición inicial de bisiles con los que se comienza el juego.
     */
    public static int MUNICION_INICIAL_MISILES = 3;

    public Misiles() {
        super(PRECIO_MISIL, MUNICION_INICIAL_MISILES);
    }

    public Misiles(int pPrecioSubclase, int pMunicionSubclase) {
        super(pPrecioSubclase, pMunicionSubclase);
    }

    /**
     * Dispara un misil a pCasilla de pFlota.
     *
     * @param pFlota
     * @param pCasilla
     * @return la lista de posiciones que han sido alcanzadas y, por tanto, reveladas.
     */
    @Override
    public Collection<Casilla> disparar(Flota pFlota, Casilla pCasilla) {
//        Collection<Casilla> casillas = null;
//        if (puedeDisparar() && Flota.casillaEnTablero(pCasilla)) {
//            abastecer(-1);
//            casillas = new ArrayList<Casilla>(1);
//            Barco barco = pFlota.barcoEnCasilla(pCasilla);
//            if (barco != null) {
//                barco.recibirImpacto();
//            }
//            casillas.add(pCasilla.getLocation());
//        }
//        return casillas;
        Collection<Casilla> casillas = null;
        if (puedeDisparar() && Flota.casillaEnTablero(pCasilla)) {
            abastecer(-1);
            casillas = new ArrayList<>(1);
            casillas.add(pCasilla);
            pFlota.recibirDisparo(casillas, b -> b.recibirImpacto());
        }
        return casillas;

    }

    /**
     * Devuelve el identificador de los misiles.
     */
    @Override
    public int getId() {
        return ID_MISILES;
    }

    @Override
    public String getNombre() {
        return "misiles";
    }

}
