package codigo.armas;

import java.util.Collection;

import codigo.Casilla;
import codigo.Flota;
import codigo.Juego;
import codigo.Orientacion;

/**
 * Esta clase representa un grupo de misiles_BOOM.
 */
public class Misiles_BOOM extends Misiles {

    /**
     * Identifcador númerico de los misiles_EO. id=4.
     */
    public final static int ID_MISILES_BOOM = 4;
    /**
     * Precio de un misil_BOOM.
     */
    public final static int PRECIO_MISIL_BOOM = 600;
    /**
     * Munición inicial de misiles_BOOM con los que se comienza el juego.
     */
    public static int MUNICION_INICIAL_MISILES_BOOM = 1;

    public Misiles_BOOM() {
        super(PRECIO_MISIL_BOOM, MUNICION_INICIAL_MISILES_BOOM);
    }

    /**
     * Dispara un misil_BOOM a pCasilla de pFlota.
     * 
     * @param pFlota
     * @param pCasilla
     * 
     * @return la lista de posiciones que han sido alcanzadas y, por tanto, reveladas.
     */
    @Override
    public Collection<Casilla> disparar(Flota pFlota, Casilla pCasilla) {
//        Collection<Casilla> casillas = null;
//        if (puedeDisparar() && Flota.casillaEnTablero(pCasilla)) {
//            abastecer(-1);
//            casillas = new ArrayList<Casilla>(2 * Juego.ALTURA_TABLERO);
//            Collection<Barco> barcos = new ArrayList<Barco>();
//            Barco barco;
//            // Horizontalmente
//            Casilla casilla = pCasilla.getLocation();
//            casilla.move((int) casilla.getX(), 0);
//            while (Flota.casillaEnTablero(casilla)) {
//                barco = pFlota.barcoEnCasilla(casilla);
//                if (barco != null && !barcos.contains(barco)) {
//                    barco.recibirImpacto();
//                    barcos.add(pFlota.barcoEnCasilla(casilla));
//                }
//                casillas.add(casilla.getLocation());
//                casilla.translate(0, 1);
//            }
//            // Verticalmente
//            casilla = pCasilla.getLocation();
//            casilla.move(0, (int) casilla.getY());
//            while (Flota.casillaEnTablero(casilla)) {
//                barco = pFlota.barcoEnCasilla(casilla);
//                if (barco != null && !barcos.contains(barco)) {
//                    barco.recibirImpacto();
//                    barcos.add(pFlota.barcoEnCasilla(casilla));
//                }
//                casillas.add(casilla.getLocation());
//                casilla.translate(1, 0);
//            }
//        }
//        return casillas;
        Collection<Casilla> casillas = null;
        if (puedeDisparar()) {
            abastecer(-1);
            casillas = Flota.encontrarCasillas(new Casilla((int) pCasilla.getX(), 0), Orientacion.ESTE, Juego.ALTURA_TABLERO);
            casillas.remove(pCasilla.getLocation());
            casillas.addAll(Flota.encontrarCasillas(new Casilla(0, (int) pCasilla.getY()), Orientacion.SUR, Juego.ALTURA_TABLERO));

            pFlota.recibirDisparo(casillas, b -> b.recibirImpacto());
        }
        return casillas;
    }

    /**
     * Devuelve el identificador de los misiles_BOOM.
     */
    @Override
    public int getId() {
        return ID_MISILES_BOOM;
    }

    @Override
    public String getNombre() {
        return "misiles BOOM";
    }

}
