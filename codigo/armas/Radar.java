package codigo.armas;

import codigo.Casilla;
import codigo.Flota;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Esta clase representa un radar.
 */
public class Radar extends Armamento {

    /**
     * Identifcador númerico del radar. id=5.
     */
    public final static int ID_RADAR = 5;
    /**
     * Munición inicial del radar con la que se comienza el juego.
     */
    public final static int MUNICION_INICIAL_RADAR = 3;

    private final static int RADIO_RADAR = 2;

    /**
     * Número de casillas que descubre el radar
     */
    private int area;

    public Radar() {
        super(-1, MUNICION_INICIAL_RADAR);
        // se calcula el area del radar
        area = 0;
        for (int i = 1; i < RADIO_RADAR; i++) {
            area += i;
        }
        area *= 4;
        area += 4 * RADIO_RADAR;
        area++;
    }

    /**
     * Dispara el Radar a pCasilla de pFlota. El radar afecta a todas las casillas dentro de su radio, pero no
     * daña los barcos, solo revela el contenido de la casilla.
     * 
     * @param pFlota
     * @param pCasilla
     * 
     * @return la lista de posiciones que han sido alcanzadas y, por tanto, reveladas.
     */
    @Override
    public Collection<Casilla> disparar(Flota pFlota, Casilla pCasilla) {
        Collection<Casilla> casillas = null;
        if (puedeDisparar() && Flota.casillaEnTablero(pCasilla)) {
            abastecer(-1);
            casillas = new ArrayList<Casilla>(area);
            Casilla casilla = pCasilla.getLocation();
            casilla.translate(-RADIO_RADAR, -RADIO_RADAR);
            for (int j = 0; j <= 2 * RADIO_RADAR; j++) {
                for (int i = 0; i <= 2 * RADIO_RADAR; i++) {
                    if (distanciaValida(casilla, pCasilla) && Flota.casillaEnTablero(casilla)) {
                        casillas.add(casilla.getLocation());
                    }
                    // se avanza una columna
                    casilla.translate(0, 1);
                }
                casilla = pCasilla.getLocation();
                casilla.translate(-RADIO_RADAR, -RADIO_RADAR);
                // se avanza una fila
                casilla.translate(j + 1, 0);
            }
        }
        return casillas;
    }

    /**
     * Devuelve el identificador del tipo concreto de armamento. Los distintos identificadores comienzan en 0
     * y van incrementando de 1 en 1. Este método no esta implementado, sino que se reescribe en las
     * subclases, cada una devolviendo su propio identificador.
     */
    @Override
    public int getId() {
        return ID_RADAR;
    }

    @Override
    public String getNombre() {
        return "el radar";
    }

    /**
     * Este tipo de armamento no se puede abastecer.
     * 
     * @param pCantidad
     *
     * @return la cantidad de munición que queda despues de la operación.
     */
    @Override
    public int abastecer(int pCantidad) {
        if (pCantidad < 0) {
            return super.abastecer(pCantidad);
        } else {
            return super.abastecer(0);
        }
    }

    /**
     * Comprueba si pPunto está a una distancia igual o menor que RADIO_RADAR de pOrigen.
     * 
     * @param pCasilla
     * @param pOrigen
     * 
     * @return true si pPunto está dentro del area que descubre el radar comenzando desde pOrigen.
     */
    private boolean distanciaValida(Casilla pCasilla, Casilla pOrigen) {
        return (Math.abs(pCasilla.getX() - pOrigen.getX())) + (Math.abs(pCasilla.getY() - pOrigen.getY())) <= RADIO_RADAR;
    }
}
