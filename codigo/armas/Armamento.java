package codigo.armas;

import codigo.Casilla;
import codigo.Flota;

import java.util.Collection;

/**
 * Esta clase representa un conjunto de armas del mismo tipo.No debe instanciarse nunca (abstracta) sino que
 * deben usarse sus subclases. Armamento tiene aceso a su precio de reabastecimineto y a la munición restante.
 */
public abstract class Armamento {
    /**
     * Cantidad de dinero que cuesta incrementas la munición en 1.
     */
    private int precio;
    /**
     * Munición restante.
     */
    private int municion;

    public Armamento(int pPrecio, int pMunicion) {
        precio = pPrecio;
        municion = pMunicion;
    }

    /**
     * Devuelve el identificador del tipo concreto de armamento. Los distintos identificadores comienzan en 0
     * y van incrementando de 1 en 1. Este método no esta implementado, sino que se reescribe en las
     * subclases, cada una devolviendo su propio identificador.
     * 
     * @return el identificador de este arma.
     */
    public int getId() {
        return -1;
    }

    /**
     * @return el precio de reabastecimiento.
     */
    public int getPrecio() {
        return precio;
    }

    /**
     * @return la munición restante.
     */
    public int getMunicion() {
        return municion;
    }

    public String getNombre() {
        return null;
    }

    /**
     * Dispara con el este arma a la casilla pCasilla de pFlota. Disparar decrementa la munición en 1 y es
     * necesario que la munición sea mayor que 0 para disparar. Este método esta sin implementar y se
     * sobreescribe en las sublases.
     * 
     * @param pFlota
     * @param pCasilla
     * 
     * @return la lista de posiciones que han sido alcanzadas y, por tanto, reveladas.
     */
    public Collection<Casilla> disparar(Flota pFlota, Casilla pCasilla) {
        throw new UnsupportedOperationException();
    }

    /**
     * @return true si se tiene suficiente munición para disparar.
     */
    public boolean puedeDisparar() {
        return municion > 0;
    }

    /**
     * Incrementa la munición en la cantidad indicada.
     *
     * @param pCantidad
     *
     * @return la cantidad de munición que queda despues de la operación.
     */
    public int abastecer(int pCantidad) {
        municion += pCantidad;
        return municion;
    }

}
